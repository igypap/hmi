#include "parameters.h"
#include "ui_parameters.h"
#include <QVector>

Parameters::Parameters(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Parameters)
{
  ui->setupUi(this);
  cMin=20.0;
  cMax=50.0;
  cWindow=0.1;
  cStep=0.1;
  status=false;
}

Parameters::~Parameters()
{
  delete ui;
}


void Parameters::on_pushButton_Ok_clicked()
{
  cMin=ui->doubleSpinBox_Range_Min->value();
  cMax=ui->doubleSpinBox_Range_Max->value();
  cWindow=ui->doubleSpinBox_Window->value();
  cStep=ui->doubleSpinBox_Step->value();
  status=true;
  emit accept();
}
QVector<double> Parameters::getParameters()
{
  QVector<double> paramsTemp(4);
  paramsTemp[0]=cMin;
  paramsTemp[1]=cMax;
  paramsTemp[2]=cWindow;
  paramsTemp[3]=cStep;
  return paramsTemp;
}

void Parameters::setParameters(QVector<double> _params)
{
  cMin=_params[0];
  cMax=_params[1];
  cWindow=_params[2];
  cStep=_params[3];
}

bool Parameters::getStatus()
{
  return status;
}


void Parameters::on_pushButton_Anuluj_clicked()
{
  emit reject();
}
