#include "mainwindow.h"
#include "konfiguracja.h"
#include <QApplication>
#include <QDebug>
#include "opencv2/opencv.hpp"
#include <cvimagewidget.h>
#include <QDialog>

#include <QMainWindow>

#include <iostream>



int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow w;
  w.setWindowState(w.windowState() | Qt::WindowMaximized);

  w.show();



  return a.exec();
}
