#include "referencedialog.h"
#include "ui_referencedialog.h"

ReferenceDialog::ReferenceDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ReferenceDialog)
{
  ui->setupUi(this);
  status=false;
  ui->radioButton_Ref_I->setChecked(true);
  //emit ui->radioButton_Ref_I->setChecked(true);
}

ReferenceDialog::~ReferenceDialog()
{
  delete ui;
}

int ReferenceDialog::getRefPathNo()
{
  return refPathNo;
}

bool ReferenceDialog::getStatus()
{
  return status;
}

void ReferenceDialog::on_pushButton_clicked()
{
  if (ui->radioButton_Ref_I->isChecked())
    refPathNo=0;
  else if (ui->radioButton_Ref_Ia->isChecked())
    refPathNo=1;
  else if (ui->radioButton_Ref_II->isChecked())
    refPathNo=2;
  status=true;
  emit accept();
}

void ReferenceDialog::on_pushButton_2_clicked()
{
  emit reject();
}
