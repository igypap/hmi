#ifndef THREADCLASS_H
#define THREADCLASS_H

#include <QThread>
#include <QtCore>
#include <opencv2/opencv.hpp>


class threadClass : public QThread
{
  Q_OBJECT

public:

  explicit threadClass(QObject *parent=0);

  void run();
  bool Stop;
  cv::Mat map1,map2;
  QVector<QVector<double>> lightCalB;
  QVector<QVector<double>> lightCalG;
  QVector<QVector<double>> lightCalR;
  int option;
  int ID;
  void setConfig(int _ID, cv::Mat _map1, cv::Mat _map2, QVector<QVector<double>> _lightCalB,
  QVector<QVector<double>> _lightCalG,
  QVector<QVector<double>> _lightCalR, int _option);

signals:
  void ImageCaptured(cv::Mat);
public slots:
};

#endif // THREADCLASS_H
