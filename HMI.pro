#-------------------------------------------------
#
# Project created by Igor Paprzycki 2016-04-13T18:40:16
#
#-------------------------------------------------

QT       += core gui
QT       += serialbus


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = HMI
TEMPLATE = app

RC_FILE = resources.rc

SOURCES += main.cpp\
        mainwindow.cpp \
        qcustomplot.cpp \
        about.cpp \
    IDSCamera.cpp \
    konfiguracja.cpp \
    threadclass.cpp \
    initthread.cpp \
    password.cpp \
    configuration.cpp \
    parameters.cpp \
    referencedialog.cpp \
    samplenodialog.cpp \
    summary.cpp


HEADERS  += mainwindow.h \
        qcustomplot.h \
        about.h \
    IDSCamera.h \
    konfiguracja.h \
    threadclass.h \
    initthread.h \
    password.h \
    configuration.h \
    parameters.h \
    referencedialog.h \
    samplenodialog.h \
    summary.h \
    version.h \
    resources.rc



FORMS    += mainwindow.ui \
         about.ui \
    konfiguracja.ui \
    password.ui \
    configuration.ui \
    parameters.ui \
    referencedialog.ui \
    samplenodialog.ui \
    summary.ui

CONFIG += static

win32:CONFIG(release, debug|release): LIBS += -LC:/OpenCV2.4.12/opencv/build/x64/vc12/lib -lopencv_calib3d2412 \
    -lopencv_contrib2412 -lopencv_core2412 -lopencv_features2d2412 -lopencv_flann2412 \
    -lopencv_gpu2412 -lopencv_highgui2412 -lopencv_imgproc2412 -lopencv_legacy2412 -lopencv_ml2412 \
    -lopencv_nonfree2412 -lopencv_objdetect2412 -lopencv_photo2412 -lopencv_stitching2412 \
    -lopencv_superres2412 -lopencv_ts2412 -lopencv_video2412 -lopencv_videostab2412
else:win32:CONFIG(debug, debug|release): LIBS += -LC:/OpenCV2.4.12/opencv/build/x64/vc12/lib -lopencv_calib3d2412d \
    -lopencv_contrib2412d -lopencv_core2412d -lopencv_features2d2412d -lopencv_flann2412d \
    -lopencv_gpu2412d -lopencv_highgui2412d -lopencv_imgproc2412d -lopencv_legacy2412d -lopencv_ml2412d \
    -lopencv_nonfree2412d -lopencv_objdetect2412d -lopencv_photo2412d -lopencv_stitching2412d \
    -lopencv_superres2412d -lopencv_ts2412d -lopencv_video2412d -lopencv_videostab2412d

INCLUDEPATH += C:/OpenCV2.4.12/opencv/build/include
DEPENDPATH += C:/OpenCV2.4.12/opencv/build/include



win32: LIBS += -LC:/IDS/uEye/Develop/Lib/ -luEye_api_64

INCLUDEPATH += C:/IDS/uEye/Develop/include
DEPENDPATH += C:/IDS/uEye/Develop/include




