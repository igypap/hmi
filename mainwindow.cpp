#include "mainwindow.h"
#include "about.h"
#include "konfiguracja.h"
#include "password.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"
#include "opencv2/opencv.hpp"
#include "configuration.h"
#include "parameters.h"
#include "referencedialog.h"
#include "samplenodialog.h"
#include "summary.h"
#include <cmath>
#include <sstream>
#include <fstream>
#include <QString>
#include <QStringList>
#include <QFileDialog>
#include <QDialog>
#include <QTextStream>
#include <QListView>
#include <QList>
#include <iterator>
#include <QtPlugin>
#include <QGraphicsView>
#include <IDSCamera.h>
#include <QModbusTcpServer>
#include <QRegularExpression>
#include <QStatusBar>
#include <QUrl>
#include <QThread>
#include <QMessageBox>
#include <QPdfWriter>
#include <QTimer>

using namespace cv;
const int GRAPH_COUNT=6; //3 wykresy dla każdego zestawu danych (pomiar i referencja) plus 4
//na analize temperaturowej odpowiedzi barwowej

bool cursorEnabled=true;
bool toReplot=false;
int serverAdd=255; //1 do testow, 255 do pracy

typedef struct
    // definicja typu zmiennych dla kursora wykresu
{
  QCPItemLine *hLine;
  QCPItemLine *vLine;
} QCPCursor;


MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , modbusDevice(Q_NULLPTR)


  // Main window initialization
{
  qRegisterMetaType<cv::Mat>("cv::Mat");
  qRegisterMetaType<QVector<QVector<double>>>("QVector<QVector<double>>");
  ui->setupUi(this);

  connect(ui->wykres, SIGNAL(mouseMove(QMouseEvent*)),
          this, SLOT(mouseRelease(QMouseEvent*)));
  connect(ui->wykres, SIGNAL(mouseRelease(QMouseEvent*)),
          this, SLOT(mouseRelease(QMouseEvent*)));
  connect(ui->wykres, SIGNAL(plottableClick(QCPAbstractPlottable*,QMouseEvent*)),
          this, SLOT(plotClick(QCPAbstractPlottable*,QMouseEvent*)));

  Data = new DataClass(this);
  Reference = new DataClass(this);
  Image1 = new ImageClass(this);
  Acq = new AcqClass(this);
  Config = new ConfigClass(this);
  wykresTemp=new QCustomPlot(this);
  dataMap = new QCPDataMap();
  dataMap0 = new QCPDataMap();
  dataMap1 = new QCPDataMap();
  dataMap2 = new QCPDataMap();
  dataMap3 = new QCPDataMap();
  dataMap4 = new QCPDataMap();
  dataMap5 = new QCPDataMap();
  dataMap6 = new QCPDataMap();
  dataMap7 = new QCPDataMap();
  dataMap8 = new QCPDataMap();
  dataMap9 = new QCPDataMap();
  states =new StatesClass(this);
  timer = new QTimer(this);
  timerReplot= new QTimer(this);

  mThread =new threadClass(this);
  connect(mThread,SIGNAL(ImageCaptured(cv::Mat)),this,SLOT(onImageCaptured(cv::Mat)));

  mInitThread=new initThread(this);
  connect(mInitThread,SIGNAL(updateProgressBar(int)),this,
          SLOT(onUpdateProgressBar(int)));
  connect(mInitThread,SIGNAL(mapsLoaded(cv::Mat,cv::Mat,
                                        QVector<QVector<double>>,QVector<QVector<double>>,QVector<QVector<double>>,
                                        QVector<QVector<double>>,QVector<QVector<double>>)),this,
          SLOT(onMapsLoaded(cv::Mat,cv::Mat,
                            QVector<QVector<double>>,QVector<QVector<double>>,QVector<QVector<double>>,
                            QVector<QVector<double>>,QVector<QVector<double>>)));
  connect(timer, SIGNAL(timeout()), this, SLOT(writePLC()));
  connect(timerReplot, SIGNAL(timeout()), this, SLOT(timeoutReplot()));


  timerReplot->start(100);
  initializeGraph(ui->wykres,wykresTemp);
  initConfig();
  initStateMachine();
  ui->frame_Test->setVisible(false);
  ui->frame->setVisible(false);
  ui->Arrow_Label->setVisible(false);
  ui->Opcja_Label->setVisible(false);
  ui->Tryb_Label->setVisible(false);
  ui->Arrow_Label->setGeometry(110,10,21,21);
  ui->Tryb_Label->setGeometry(130,10,181,21);

}


void MainWindow::initStateMachine()
{
  //opisac wszystkie stany i oprogramowac funkcje do nich

  machine = new QStateMachine(this);

  stateInit = new QState(machine);
  stateIdle = new QState(machine);
  stateStartCalibrate = new QState(machine);
  stateSendSetpoints = new QState(machine);
  stateSetpointsSent = new QState(machine);
  stateLightsOn = new QState(machine);
  stateCaptureImgCal = new QState(machine);
  stateImageCaptured = new QState(machine);
  stateIsCalibrateFinished = new QState(machine);

  stateInit->addTransition(this, SIGNAL(changeState()), stateIdle);
  stateIdle->addTransition(this, SIGNAL(changeState()), stateStartCalibrate);
  stateIdle->addTransition(this, SIGNAL(changeStateToInit()), stateInit);
  stateStartCalibrate->addTransition(this, SIGNAL(changeState()), stateSendSetpoints);
  stateStartCalibrate->addTransition(this, SIGNAL(changeStateToInit()), stateInit);
  stateSendSetpoints->addTransition(this, SIGNAL(changeState()), stateSetpointsSent);
  stateSendSetpoints->addTransition(this, SIGNAL(changeStateToInit()), stateInit);

  stateSetpointsSent->addTransition(this, SIGNAL(changeState()), stateLightsOn);
  stateSetpointsSent->addTransition(this, SIGNAL(changeStateToInit()), stateInit);


  stateLightsOn->addTransition(this, SIGNAL(changeState()), stateCaptureImgCal);
  stateLightsOn->addTransition(this, SIGNAL(changeStateToInit()), stateInit);
  stateCaptureImgCal->addTransition(this, SIGNAL(changeState()), stateImageCaptured);
  stateCaptureImgCal->addTransition(this, SIGNAL(changeStateToInit()), stateInit);
  stateImageCaptured->addTransition(this, SIGNAL(changeState()), stateIsCalibrateFinished);
  stateImageCaptured->addTransition(this, SIGNAL(changeStateToInit()), stateIdle);
  stateIsCalibrateFinished->addTransition(this, SIGNAL(changeState()), stateIdle);
  stateIsCalibrateFinished->addTransition(this, SIGNAL(changeStateToInit()), stateInit);
  stateIsCalibrateFinished->addTransition(this, SIGNAL(changeStateToSendSetpoints()), stateSendSetpoints);

  machine->setInitialState(stateInit);
  machine->start();
  QObject::connect(stateInit, SIGNAL(entered()), this, SLOT(stateInitSlot()));
  QObject::connect(stateIdle, SIGNAL(entered()), this, SLOT(stateIdleSlot()));
  QObject::connect(stateStartCalibrate, SIGNAL(entered()), this, SLOT(stateSTartCalibrateSlot()));
  QObject::connect(stateSendSetpoints, SIGNAL(entered()), this, SLOT(stateSendSetpointsSlot()));
  QObject::connect(stateLightsOn, SIGNAL(entered()), this, SLOT(stateLightsOnSlot()));
  QObject::connect(stateCaptureImgCal, SIGNAL(entered()), this, SLOT(stateCaptureImgCalSlot()));
  QObject::connect(stateImageCaptured, SIGNAL(entered()), this, SLOT(stateImageCapturedSlot()));
  QObject::connect(stateIsCalibrateFinished, SIGNAL(entered()), this, SLOT(stateIsCalibrateFinishedSlot()));
}


void MainWindow::updateStates()
{

  /* obsługa stanów na podstawie maszyny stanów machine*/
  if (Acq->PLCState[3]==1)
    {
      QMessageBox::information(this,"Drzwi otwarte","Zamknij drzwi komory aby kontunuować");
      Acq->PLCState[3]==0;
      emit changeStateToInit();
    }
  else if(Acq->PLCState[4]==1)
    {
      QMessageBox::critical(this,"Awaria!","Awaria urządzenia. Nie można kontynuować pracy");
      Acq->PLCState[4]==0;
      emit changeStateToInit();
    }
  else{
      if (stateInit->active())
        {

        }
      if (stateIdle->active())
        {

        }

      if (stateStartCalibrate->active()&&Acq->PLCState[1]==1)
        //PLCState[1]==1 - gotowość do pracy PLC,
        {
          emit changeState();
        }

      if (stateSendSetpoints->active()&&Acq->PLCState[0]==1)
        //PLCState[0]==1 - temperatura osiągnięta,
        {
          // emit changeState();
        }
      else if (stateSendSetpoints->active()&&Acq->PLCState[1]==0){
          //PLCState[1]==0 - brak gotowości do pracy PLC,
          emit changeStateToInit();
        }

      if (stateSetpointsSent->active()&&Acq->PLCState[0]==1)
        //PLCState[0]==1 - temperatura osiągnięta,
        {
          emit changeState();
        }
      else if (stateSetpointsSent->active()&&Acq->PLCState[1]==0){
          //PLCState[1]==0 - brak gotowości do pracy PLC,
          emit changeStateToInit();
        }

      if (stateLightsOn->active()&&Acq->PLCState[2]==1)
        //PLCState[2]==1 - potwierdzenie z PLC o wlaczeniu swiatla,

        {
          emit changeState();
        }
      else if (stateLightsOn->active()&&Acq->PLCState[1]==0){
          //PLCState[1]==0 - brak gotowości do pracy PLC,,
          emit changeStateToInit();
        }

      if (stateCaptureImgCal->active())
        {
          //warunek transition dla wykonanego zdjecia obslugiwany jest przez SIGNAL generowany przez przerwanie
          //wykonujace zdjecie
        }
    }
}


void MainWindow::stateInitSlot()
{
  MainWindow::ui->label_21->setText("Inicjalizacja zakończona");
  Config->plcRegisters[0]=1;
  Config->plcRegisters[1]=0;
  Config->plcRegisters[2]=0;
  Config->plcRegisters[3]=0;
  Config->plcRegisters[4]=0;
  //  setRegister(1,0);
  //  setRegister(0,1);
  //  setRegister(0,2);
  //  setRegister(0,3);
  states->currentState=0;
  if (states->screen==1) //1 - Test, 2 - Kalibracja
    {
      Data->isData=false;
      Reference->isData=false;
      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      setVisible("Test");
      states->currentState=0;

    }
  else if (states->screen==2)
    {
      Data->isData=false;
      Reference->isData=false;
      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      setVisible("Kalibracja");
      states->currentState=0;

      ui->calibrateButton->setEnabled(true);
    }
  emit changeState();

}

void MainWindow::stateIdleSlot()
{
  MainWindow::ui->label_21->setText("Bezczynny");
  Config->plcRegisters[1]=0;
  Config->plcRegisters[2]=0;
  Config->plcRegisters[3]=0;
  Config->plcRegisters[4]=0;
}

void MainWindow::stateSTartCalibrateSlot()
{
  Config->plcRegisters[1]=1;

}

void MainWindow::stateSendSetpointsSlot()
{
  ui->label_21->setText("Oczekiwanie na stabilizację temperatury...");
  int ind=states->currentState; //nr kolejnego zdjecia do wykonania
  int tempMIN=Config->min[ind]*1000;
  int tempMAX=Config->max[ind]*1000;
  Config->plcRegisters[2]=tempMIN;
  Config->plcRegisters[3]=tempMAX;
  ui->gradientButton->setEnabled(false);
  ui->calibrateButton->setEnabled(false);
  emit changeState();
}

void MainWindow::stateLightsOnSlot()
{
  MainWindow::ui->label_21->setText("Oczekiwanie na włączenie oświetlenia...");
  poly(Acq,2); //obliczenie wielomianu
  Config->plcRegisters[4]=1; //wyslij do PLC polecenie wlaczenia swiatla
}

void MainWindow::stateCaptureImgCalSlot()
{
  MainWindow::ui->label_21->setText("Pobieranie obrazu z kamery...");
  ui->calibrateButton->setEnabled(false);
  ui->SaveFileButton->setEnabled(false);
  ui->menuPlik->setEnabled(false);
  ui->menuOpcje->setEnabled(false);
  ui->wyczyscButton->setEnabled(false);
  ui->zastapButton->setEnabled(false);
  getImage();
}

void MainWindow::stateImageCapturedSlot()
{
  MainWindow::ui->label_21->setText("Obraz z kamery pobrany");
  if (states->screen==2)
    //ekran kalibracja
    {
      captureCalibrate();
      ui->menuPlik->setEnabled(true);
      ui->menuOpcje->setEnabled(true);
      ui->wyczyscButton->setEnabled(true);
    }
  else if (states->screen==1)
    //ekran Test
    {
      captureImg(true);
      ui->menuPlik->setEnabled(true);
      ui->menuOpcje->setEnabled(true);
      ui->wyczyscButton->setEnabled(true);
    }
}

void MainWindow::stateIsCalibrateFinishedSlot()
{
  if (states->screen==2)
    //ekran kalibracja
    {
      if (states->currentState==Config->max.size()-1)
        //koniec kalibracji
        {
          Config->plcRegisters[1]=0;
          Config->plcRegisters[2]=0;
          Config->plcRegisters[3]=0;
          Config->plcRegisters[4]=0;
          MainWindow::ui->label_21->setText("Kalibracja zakończona");
          sort(Reference,0,Reference->xTemp.size()-1);
          MeanData(Reference,10);
          SaveToCSV(this,Reference,"");
          ui->SaveFileButton->setEnabled(true);
          ui->gradientButton->setEnabled(true);
          ui->calibrateButton->setEnabled(true);
          initializeGraph(ui->wykres,wykresTemp);
          print(ui->wykres,Reference,2);
          states->currentState=0;
          emit changeState();
        }
      else
        states->currentState+=1;

      emit changeStateToSendSetpoints();
    }
  else if (states->screen==1)
    //ekran test
    {
      MainWindow::ui->label_21->setText("Obraz z kamery przetworzony");
      QString fileName=Config->dataPaths[0]+"/"+QDate::currentDate().toString(Qt::ISODate)+"_"
          + QTime::currentTime().toString("hh_mm_ss")+"_"+Config->sampleNo;
      Data->name=fileName.section("/",-1,-1);
      Data->path=fileName.section("/",0,-2);
      SaveToCSV(this,Data,fileName+".csv");

      Summary newSummary(this);
      newSummary.setErrors(Config->dTr,Config->dTg,Config->dTh,Config->dTb,Config->result);
      newSummary.setModal(true);
      newSummary.exec();

      ui->gradientButton->setEnabled(true);
      states->currentState=0;
      Config->plcRegisters[1]=0;
      Config->plcRegisters[2]=0;
      Config->plcRegisters[3]=0;
      Config->plcRegisters[4]=0;

      emit changeState();
    }

}


MainWindow::~MainWindow()
//Main window destructor
{
  if (modbusDevice)
    modbusDevice->disconnectDevice();
  delete modbusDevice;
  delete ui;
}

void MainWindow::initConfig()
{

  setVisible("Nothing");
  ui->menuBar->setVisible(false);
  openConfigFile();

  if (serverAdd==255)
    getMaps();
  else{
      initModbus();
      setVisible("Test");
      ui->label_Progress->setVisible(false);
      ui->actionKonfiguracja->setVisible(false);
      ui->actionKalibracja->setVisible(false);
      ui->menuBar->setVisible(true);
    }

}


StatesClass::StatesClass(QObject *parent)
{
  screen=0;
  statePLC=0;
  currentState=0;
  imageCaptured=false;
}

StatesClass::~StatesClass()
{
  //delete states;
}

ConfigClass::ConfigClass(QObject *parent)
//konstruktor inicjujący zmienne klasy
{
  lightCalOption=0;
  temperatureCalOption=0;
  QVector<QVector<double>> emptyCalVector(0);
  lightCalB = emptyCalVector;
  lightCalG = emptyCalVector;
  lightCalR = emptyCalVector;
  temperatureCalA = emptyCalVector;
  temperatureCalB = emptyCalVector;
  map1.create(2048, 2048, CV_32FC1);
  map2.create(2048, 2048, CV_32FC1);

  QVector<QString> tmp(2);
  tmp[0]=QString("");
  tmp[1]=QString("");
  QVector<double> tmpInt(12,0);
  paths=tmp;
  dataPaths=tmp;
  tmp.append("");
  refPaths=tmp;
  pixels=tmpInt;
  sampleNo="";
  orderNo="";
  status=false;
  ID_Cam=0;
  H_Tr=0.000;
  S_Tr=0.000;
  H_Tg=0.000;
  H_Th=0.000;
  H_Tb=0.000;
  HD_Tr_H=0.000;
  HD_Tr_HH=0.000;
  HD_Tg_H=0.000;
  HD_Tg_HH=0.000;
  HD_Th_H=0.000;
  HD_Th_HH=0.000;
  HD_Tb_H=0.000;
  HD_Tb_HH=0.000;
  dTr=0.0;
  dTg=0.0;
  dTh=0.0;
  dTb=0.0;

  refTr=0.0;
  refTg=0.0;
  refTh=0.0;
  refTb=0.0;

  pomTr=0.0;
  pomTg=0.0;
  pomTh=0.0;
  pomTb=0.0;

  dTrError=0.0; //0 - zielony, 1- żółty, 2-czerwony
  dTgError=0.0;
  dThError=0.0;
  dTbError=0.0;
  result="";
  password="";
  cMin=0.0;
  cMax=0.0;
  cWindow=0.0;
  cStep=0.0;
  QVector<int> tmpVec(5);
  for (int i=0;i<tmpVec.size();i++){
      tmpVec[i]=0;
    }
  plcRegisters=tmpVec;

}


ConfigClass::~ConfigClass()
{
  //delete Config;
}

void MainWindow::openConfigFile()
{
  QString fileName = QDir::currentPath()+"/Config/HMIconfig.ini";
  QSettings settings(fileName, QSettings::IniFormat);

  if (QFile(fileName).exists()) //plik konfiguracyjny istnieje w systemie
    {
      QVector<double> pixelsTemp(12);
      for (int n=0; n<12; n++)
        //odczyt 12 wartości pixeli
        {
          pixelsTemp[n]=settings.value("pixels/Temperature_"+QString::number(n+1)+"_pixel_no")
              .toDouble();
        }
      Config->pixels=pixelsTemp;
      Acq->x=Config->pixels;
      Config->ID_Cam=settings.value("IDCam/ID_Cam").toInt();
      QVector<QString> tmpStr(2);

      tmpStr[0]=/*QDir::currentPath()+*/settings.value("dataPaths/data").toString();
      tmpStr[1]=/*QDir::currentPath()+*/settings.value("dataPaths/report").toString();
      Config->dataPaths=tmpStr;
      tmpStr.append("");
      tmpStr[0]=/*QDir::currentPath()+*/settings.value("references/ref_I").toString();
      tmpStr[1]=/*QDir::currentPath()+*/settings.value("references/ref_Ia").toString();
      tmpStr[2]=/*QDir::currentPath()+*/settings.value("references/ref_II").toString();
      Config->refPaths=tmpStr;
      tmpStr.append("");
      tmpStr.append("");
      tmpStr.append("");
      tmpStr.append("");
      tmpStr[0]=/*QDir::currentPath()+*/settings.value("maps/map1_path").toString();
      tmpStr[1]=/*QDir::currentPath()+*/settings.value("maps/map2_path").toString();
      tmpStr[2]=/*QDir::currentPath()+*/settings.value("maps/lightCalB_path").toString();
      tmpStr[3]=/*QDir::currentPath()+*/settings.value("maps/lightCalG_path").toString();
      tmpStr[4]=/*QDir::currentPath()+*/settings.value("maps/lightCalR_path").toString();
      tmpStr[5]=/*QDir::currentPath()+*/settings.value("maps/temperatureCalA_path").toString();
      tmpStr[6]=/*QDir::currentPath()+*/settings.value("maps/temperatureCalB_path").toString();
      Config->paths=tmpStr;

      Config->H_Tr=settings.value("HErrors/H_Tr").toDouble();
      Config->S_Tr=settings.value("HErrors/S_Tr").toDouble();
      Config->H_Tg=settings.value("HErrors/H_Tg").toDouble();
      Config->H_Th=settings.value("HErrors/H_Th").toDouble();
      Config->H_Tb=settings.value("HErrors/H_Tb").toDouble();
      Config->HD_Tr_H=settings.value("HErrors/HD_Tr_H").toDouble();
      Config->HD_Tr_HH=settings.value("HErrors/HD_Tr_HH").toDouble();
      Config->HD_Tg_H=settings.value("HErrors/HD_Tg_H").toDouble();
      Config->HD_Tg_HH=settings.value("HErrors/HD_Tg_HH").toDouble();
      Config->HD_Th_H=settings.value("HErrors/HD_Th_H").toDouble();
      Config->HD_Th_HH=settings.value("HErrors/HD_Th_HH").toDouble();
      Config->HD_Tb_H=settings.value("HErrors/HD_Tb_H").toDouble();
      Config->HD_Tb_HH=settings.value("HErrors/HD_Tb_HH").toDouble();
      Config->Ref_I_MIN = settings.value("Defaults/RefIMIN").toDouble();
      Config->Ref_I_MAX = settings.value("Defaults/RefIMAX").toDouble();
      Config->Ref_Ia_MIN = settings.value("Defaults/RefIaMIN").toDouble();
      Config->Ref_Ia_MAX = settings.value("Defaults/RefIaMAX").toDouble();
      Config->Ref_II_MIN = settings.value("Defaults/RefIIMIN").toDouble();
      Config->Ref_II_MAX = settings.value("Defaults/RefIIMAX").toDouble();
      Config->password=settings.value("Password/pass").toString();

      double cMin=settings.value("CalibrateParams/C_Min").toDouble();
      double cMax=settings.value("CalibrateParams/C_Max").toDouble();
      double cWindow=settings.value("CalibrateParams/C_Window").toDouble();
      double cStep=settings.value("CalibrateParams/C_Step").toDouble();
      Config->cMin=cMin;
      Config->cMax=cMax;
      Config->cWindow=cWindow;
      Config->cStep=cStep;
      Config->lightCalOption=settings.value("lightTemperatureCorect/light").toInt();
      Config->temperatureCalOption=settings.value("lightTemperatureCorect/temperature").toInt();

      int i=0;
      QVector<double>minVec,maxVec;
      while(cMin+i*cStep+cWindow<=cMax&&i<100)
        {
          minVec.append(cMin+i*cStep);
          maxVec.append(cMin+i*cStep+cWindow);
          i++;
        }
      Config->min=minVec;
      Config->max=maxVec;
      Config->status=true;


    }
  else //błąd otwarcia pliku konfiguracyjnego
    {
      QMessageBox::critical(this,"Brak konfiguracji",
                            "Nie można załadować pliku konfiguracyjnego 'HMIconfig.ini'. "
                            "Aplikacja zostanie zamknięta. Skontaktuj się z administratorem Systemu Kalibracyjnego");
      emit quick_exit(-1);//zamknięcie aplikacji

    }

}

void MainWindow::saveConfigFile()
{


  QString fileName = QDir::currentPath()+"/Config/HMIconfig.ini";
  QSettings settings(fileName, QSettings::IniFormat);

  if (QFile(fileName).exists()) //plik konfiguracyjny istnieje w systemie
    {
      settings.setValue("references/ref_I",Config->refPaths[0]);
      settings.setValue("references/ref_Ia",Config->refPaths[1]);
      settings.setValue("references/ref_II",Config->refPaths[2]);

      settings.setValue("dataPaths/data",Config->dataPaths[0]);
      settings.setValue("dataPaths/report",Config->dataPaths[1]);

      settings.setValue("HErrors/H_Tr",Config->H_Tr);
      settings.setValue("HErrors/S_Tr",Config->S_Tr);
      settings.setValue("HErrors/H_Tg",Config->H_Tg);
      settings.setValue("HErrors/H_Th",Config->H_Th);
      settings.setValue("HErrors/H_Tb",Config->H_Tb);

      settings.setValue("HErrors/HD_Tr_H",Config->HD_Tr_H);
      settings.setValue("HErrors/HD_Tr_HH",Config->HD_Tr_HH);
      settings.setValue("HErrors/HD_Tg_H",Config->HD_Tg_H);
      settings.setValue("HErrors/HD_Tg_HH",Config->HD_Tg_HH);
      settings.setValue("HErrors/HD_Th_H",Config->HD_Th_H);
      settings.setValue("HErrors/HD_Th_HH",Config->HD_Th_HH);
      settings.setValue("HErrors/HD_Tb_H",Config->HD_Tb_H);
      settings.setValue("HErrors/HD_Tb_HH",Config->HD_Tb_HH);

      settings.setValue("Password/pass",Config->password);

      Config->password=settings.value("Password/pass").toString();

      double cMin=settings.value("CalibrateParams/C_Min").toDouble();
      double cMax=settings.value("CalibrateParams/C_Max").toDouble();
      double cWindow=settings.value("CalibrateParams/C_Window").toDouble();
      double cStep=settings.value("CalibrateParams/C_Step").toDouble();
      QVector<double> params;
      params.append(cMin);
      params.append(cMax);
      params.append(cWindow);
      params.append(cStep);
      Config->calcGradientVecs(params);

      Config->status=true;

    }
  else{
      QMessageBox::warning(this,"Błąd","Nie można zapisać danych do pliku konfiguracyjnego");

    }
}

void ConfigClass::calcGradientVecs(QVector<double> _params)
//w trybie wykonywania wzorca, okno dialogowe zapyta się o zakres, okno i krok
//na tej podstawie wyliczony zostanie vector gradientów min i max
//ponieważ w ten sposób jest obslugiwana praca automatyczna
{
  double _cMin, _cMax, _cWindow, _cStep;
  _cMin=_params[0];
  _cMax=_params[1];
  _cWindow=_params[2];
  _cStep=_params[3];
  int i=0;
  QVector<double>minVec,maxVec;
  while(_cMin+i*_cStep+_cWindow<=_cMax&&i<100)
    {
      minVec.append(_cMin+i*_cStep);
      maxVec.append(_cMin+i*_cStep+_cWindow);
      i++;
    }
  min=minVec;
  max=maxVec;
}

void MainWindow::getMaps()
{
  ui->progressBar->setVisible(true);
  mInitThread->setPaths(Config->paths);
  mInitThread->start();

}

void ConfigClass::setMaps(cv::Mat _map1, cv::Mat _map2)
{
  map1=_map1;
  map2=_map2;
}

DataClass::DataClass(QObject *parent)
{
  isData=false;
  path= name= date= time="";
}

DataClass::~DataClass()
{
  //delete Data;
  //delete Reference;
}

void DataClass::clear()
{
  this->path="";
  this->name="";
  this->xTemp.clear();
  this->yH.clear();
  this->yS.clear();
  this->yV.clear();
  this->isData=false;

}


ImageClass::~ImageClass()
{
  //delete Image1;
}

ImageClass::ImageClass(QObject *parent)
{

}

AcqClass::AcqClass(QObject *parent)
{
  QVector<int> tmp(10,0);
  QVector<double> tmp2(12,0);
  PLCState=tmp;
  y=tmp2;
}

AcqClass::~AcqClass()
{
  //delete Acq;
}


void AcqClass::setData(QVector<double> _yT, QVector<double> _x)
{
  x=_x;
  y=_yT;
}

void MainWindow::readPLC()
//komunikacja modbus i odczyt danych
{

}

void MainWindow::writePLC()
{
  //petla wysylająca dane do PLC
  for (int i=0;i<Config->plcRegisters.size();i++){
      setRegister(Config->plcRegisters[i],i);
    }
  updateStates();
  //ui->wykres->replot();

}

void MainWindow::timeoutReplot()
{
  ui->wykres->replot();
}

void MainWindow::poly(AcqClass *Acq, int degree)
//
{
  int N=Acq->x.size();
  QVector<double> x(N),y(N);
  x=Acq->x;

  //    //czy wykonywać korekcję oświetlenia? jeżeli 1 to tak (pobrane z pliku HMIConfig.ini)
  if(Config->temperatureCalOption == 1){
      y=calculateCorrectedTemperatureVector(Acq->y,Acq->ambientTemperature);
    }
  else{
      y=Acq->y;
    }
  int n=degree; // n is the degree of Polynomial
  QVector<double> X(2*n+1); //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
  for (int i=0;i<2*n+1;i++)
    {
      X[i]=0;
      for (int j=0;j<N;j++)
        X[i]=X[i]+pow(x[j],i); //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    }
  QVector<QVector<double>> B(n+1);
  for (int i=0;i<n+1;i++)
    {
      B[i].resize(n+2);
    }
  QVector<double>    a(n+1); //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
  for (int i=0;i<=n;i++)
    for (int j=0;j<=n;j++)
      B[i][j]=X[i+j]; //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
  QVector<double> Y(n+1); //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
  for (int i=0;i<n+1;i++)
    {
      Y[i]=0;
      for (int j=0;j<N;j++)
        Y[i]=Y[i]+pow(x[j],i)*y[j]; //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
    }
  for (int i=0;i<=n;i++)
    B[i][n+1]=Y[i]; //load the values of Y as the last column of B(Normal Matrix but augmented)
  n=n+1; //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations

  for (int i=0;i<n;i++) //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
    for (int k=i+1;k<n;k++)
      if (B[i][i]<B[k][i])
        for (int j=0;j<=n;j++)
          {
            double temp=B[i][j];
            B[i][j]=B[k][j];
            B[k][j]=temp;
          }

  for (int i=0;i<n-1;i++) //loop to perform the gauss elimination
    for (int k=i+1;k<n;k++)
      {
        double t=B[k][i]/B[i][i];
        for (int j=0;j<=n;j++)
          B[k][j]=B[k][j]-t*B[i][j]; //make the elements below the pivot elements equal to zero or elimnate the variables
      }
  for (int i=n-1;i>=0;i--)                //back-substitution
    {                        //x is an array whose values correspond to the values of x,y,z..
      a[i]=B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
      for (int j=0;j<n;j++)
        if (j!=i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
          a[i]=a[i]-B[i][j]*a[j];
      a[i]=a[i]/B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
    }

  Acq->polyCoef=a;
}

QImage Mat2QImage(const cv::Mat3b &src)
//konwersja obrazu MAT na QImage aby go wyświetlić w obiekcie QWidget Label
{
  QImage dest(src.cols, src.rows, QImage::Format_ARGB32);
  for (int y = 0; y < src.rows; ++y)
    {
      const cv::Vec3b *srcrow = src[y];
      QRgb *destrow = (QRgb*)dest.scanLine(y);
      for (int x = 0; x < src.cols; ++x)
        {
          destrow[x] = qRgba(srcrow[x][2], srcrow[x][1], srcrow[x][0], 255);
        }
    }
  return dest;
}




void MainWindow::setVisible(QString tryb)
{
  if (tryb=="Nothing")
    {
      ui->pushButton->setVisible(false);
      ui->pushZrobZdjecie->setVisible(true);
      states->screen=0;
      ui->line->setVisible(false);
      ui->progressBar->setVisible(false);
      ui->SaveFileButton->setEnabled(false);
      ui->SaveFileButton->setVisible(false);
      ui->wykres->setVisible(false);
      ui->listWidget->setVisible(false);
      ui->OpenFileButton->setVisible(false);
      ui->openMat->setVisible(false);
      ui->OpenReference->setVisible(false);
      ui->comboBox->setVisible(false);
      ui->Xlabel->setVisible(false);
      ui->Xlabel_2->setVisible(false);
      ui->Ylabel->setVisible(false);
      ui->Ylabel_2->setVisible(false);
      ui->plusButton_2->setVisible(false);
      ui->minusButton_2->setVisible(false);
      ui->zeroButton_2->setVisible(false);
      ui->frameAnaliza->setVisible(false);
      ui->wyczyscButton->setVisible(false);
      ui->zastapButton->setVisible(false);
      ui->labelINFO->setVisible(false);
      ui->labelMIN->setVisible(false);
      ui->labelMAX->setVisible(false);
      ui->lineEditMAX->setVisible(false);
      ui->lineEditMIN->setVisible(false);
      ui->gradientButton->setVisible(false);
      ui->gradientButton->setEnabled(false);
      ui->labelGradient->setVisible(false);
      ui->imgLabelRGB->clear();
      ui->imgLabelHSV->clear();
      ui->imgLabelRGB->setVisible(false);
      ui->imgLabelHSV->setVisible(false);
      ui->imgLabel1->setVisible(false);
      ui->imgLabel2->setVisible(false);
      ui->captureImg->setVisible(false);
      ui->calibrateButton->setVisible(false);
      ui->reportButton->setEnabled(false);
      ui->reportButton->setVisible(false);
      ui->Arrow_Label->setVisible(false);
      ui->Opcja_Label->setVisible(false);
      ui->Tryb_Label->setVisible(false);

    }

  if (tryb=="Test")
    {
      setVisible("Nothing");
      states->screen=1;
      ui->line->setVisible(true);
      ui->OpenReference->setVisible(true);
      ui->OpenReference->setEnabled(true);
      ui->wykres->setVisible(true);
      ui->listWidget->setVisible(true);
      ui->Xlabel->setVisible(true);
      ui->Xlabel_2->setVisible(true);
      ui->Ylabel->setVisible(true);
      ui->Ylabel_2->setVisible(true);
      ui->plusButton_2->setVisible(true);
      ui->minusButton_2->setVisible(true);
      ui->zeroButton_2->setVisible(true);
      ui->frameAnaliza->setVisible(true);
      ui->wyczyscButton->setVisible(true);
      ui->labelMIN->setVisible(true);
      ui->labelMAX->setVisible(true);
      ui->lineEditMAX->setVisible(true);
      ui->lineEditMIN->setVisible(true);
      ui->gradientButton->setVisible(true);
      ui->labelGradient->setVisible(true);
      ui->imgLabelRGB->setVisible(true);
      ui->imgLabelHSV->setVisible(true);
      ui->imgLabel1->setVisible(true);
      ui->imgLabel2->setVisible(true);
      ui->reportButton->setVisible(true);

      ui->Arrow_Label->setVisible(true);
      ui->Opcja_Label->setVisible(true);
      ui->Opcja_Label->setText("Tryb produkcyjny");
      ui->Arrow_Label->setGeometry(140,10,21,21);
      ui->Tryb_Label->setGeometry(160,10,181,21);
      ui->Tryb_Label->setVisible(true);
      ui->Tryb_Label->setText("Badanie jednokrotne");


    }
  if (tryb=="Kalibracja")
    {
      setVisible("Nothing");
      states->screen=2;
      ui->line->setVisible(true);
      ui->calibrateButton->setVisible(true);
      ui->listWidget->setVisible(true);
      ui->wykres->setVisible(true);
      // ui->comboBox->setVisible(true);
      ui->Xlabel->setVisible(true);
      ui->Xlabel_2->setVisible(true);
      ui->Ylabel->setVisible(true);
      ui->Ylabel_2->setVisible(true);
      ui->plusButton_2->setVisible(true);
      ui->minusButton_2->setVisible(true);
      ui->zeroButton_2->setVisible(true);
      ui->frameAnaliza->setVisible(true);
      ui->wyczyscButton->setVisible(true);
      ui->imgLabelRGB->setVisible(true);
      ui->imgLabelHSV->setVisible(true);
      ui->imgLabel1->setVisible(true);
      ui->imgLabel2->setVisible(true);
      ui->SaveFileButton->setVisible(true);
      ui->SaveFileButton->setEnabled(false);
      ui->Arrow_Label->setVisible(true);
      ui->Opcja_Label->setVisible(true);
      ui->Tryb_Label->setVisible(true);
      ui->Tryb_Label->setText("Wykonywanie wzorca");

    }
  if (tryb=="Analiza")
    {
      setVisible("Nothing");
      states->screen=3;
      ui->line->setVisible(true);
      ui->wykres->setVisible(true);
      ui->listWidget->setVisible(true);
      ui->OpenFileButton->setVisible(true);
      ui->OpenFileButton->setEnabled(true);
      ui->OpenReference->setVisible(true);
      ui->OpenReference->setEnabled(true);
      //ui->comboBox->setVisible(true);
      ui->Xlabel->setVisible(true);
      ui->Xlabel_2->setVisible(true);
      ui->Ylabel->setVisible(true);
      ui->Ylabel_2->setVisible(true);
      ui->plusButton_2->setVisible(true);
      ui->minusButton_2->setVisible(true);
      ui->zeroButton_2->setVisible(true);
      ui->frameAnaliza->setVisible(true);
      ui->wyczyscButton->setVisible(true);
      //ui->zastapButton->setVisible(true);
      ui->imgLabelRGB->setVisible(true);
      ui->imgLabelHSV->setVisible(true);
      ui->imgLabel1->setVisible(true);
      ui->imgLabel2->setVisible(true);
      ui->reportButton->setVisible(true);
      ui->Arrow_Label->setVisible(true);
      ui->Opcja_Label->setVisible(true);
      ui->Tryb_Label->setVisible(true);
      ui->Tryb_Label->setText("Porównywanie wyników");
    }
}

void ManageCursor(QCustomPlot *customPlot, QCPCursor *cursor, double x, double y, QPen pen)
//rysowanie kursora na wykresie
{
  if(cursorEnabled)
    {
      if(cursor->hLine)
        customPlot->removeItem(cursor->hLine);
      cursor->hLine = new QCPItemLine(customPlot);
      customPlot->addItem(cursor->hLine);
      cursor->hLine->setPen(pen);
      cursor->hLine->start->setCoords(QCPRange::minRange, y);
      cursor->hLine->end->setCoords(QCPRange::maxRange, y);

      if(cursor->vLine)
        customPlot->removeItem(cursor->vLine);
      cursor->vLine = new QCPItemLine(customPlot);
      customPlot->addItem(cursor->vLine);
      cursor->vLine->setPen(pen);
      cursor->vLine->start->setCoords( x, QCPRange::minRange);
      cursor->vLine->end->setCoords( x, QCPRange::maxRange);
    }
}

void MainWindow::plotClick(QCPAbstractPlottable* plottable, QMouseEvent* event)

{
  if (event->button()==Qt::LeftButton)
    {
      dataMap=ui->wykres->selectedGraphs().first()->data();

    }

}

void MainWindow::mouseRelease(QMouseEvent* event)
//event handler dla mouse button release. Przekazuje koordynaty x,y i rysuje kursor po wykresie
{

  QCustomPlot *customPlot=ui->wykres;
  static QCPCursor cursor1;
  double x=customPlot->xAxis->pixelToCoord(event->pos().x());
  double y=customPlot->yAxis->pixelToCoord(event->pos().y());

  if(event->button() == Qt::LeftButton)
    //event handler dla lewego przycisku myszy
    {
      ManageCursor(customPlot, &cursor1, x, y, QPen(Qt::red));
      ui->Xlabel->setText(QString::number(qRound(x*100.0)/100.0,'f',2));
      ui->Ylabel->setText(QString::number(qRound(y*100.0)/100.0,'f',2));
    }
  else if(!(customPlot->selectedGraphs().isEmpty()))
    //event handler dla ruchu myszy gdy zaznaczony jest wykres
    {
      ui->labelINFO->setVisible(false);

      //znajdź wartość Y zaznaczonego wykresu dla poruszającego się kursora X:
      if (x>=dataMap->first().key&&x<dataMap->last().key)
        //sprawdza czy kursor porusza się w zakresie danych osi X
        {
          y=dataMap->lowerBound(x).value().value;
          ui->Ylabel->setText(QString::number(qRound(y*100.0)/100.0,'f',2));
          ManageCursor(customPlot, &cursor1, x, y, QPen(Qt::red));
          ui->Xlabel->setText(QString::number(qRound(x*100.0)/100.0,'f',2));
          if (!ui->lineEditTEMP->isEnabled())
            ui->lineEditTEMP->setValue(x);
        }
      else if (x<dataMap->first().key)
        //jeśli kursor jest poniżej wartości X
        {
          y=dataMap->first().value;
          x=dataMap->first().key;
          ui->Ylabel->setText(QString::number(qRound(y*100.0)/100.0,'f',2));
          ui->Xlabel->setText(QString::number(qRound(x*100.0)/100.0,'f',2));
          ManageCursor(customPlot, &cursor1, dataMap->first().key, dataMap->first().value, QPen(Qt::black));


        }
      else if (x>=dataMap->last().key)
        //jeśli kursor jest powyżej wartości X
        {
          y=dataMap->last().value;
          x=dataMap->last().key;
          ui->Ylabel->setText(QString::number(qRound(y*100.0)/100.0,'f',2));
          ui->Xlabel->setText(QString::number(qRound(x*100.0)/100.0,'f',2));
          ManageCursor(customPlot, &cursor1, dataMap->last().key, dataMap->last().value, QPen(Qt::black));
        }
      toReplot=true;
    }
  else
    //pokazanie INFO przy poruszającym się kursorze
    {
      ui->labelINFO->setVisible(true);
      int x1, y1;
      QPoint p = customPlot->mapFromGlobal(QCursor::pos());
      x1=p.x();
      y1=p.y();

      ui->labelINFO->setGeometry(x1+50,y1,182,16);
      ManageCursor(customPlot, &cursor1, x, y, QPen(Qt::black));
      ui->Xlabel->setText(QString::number(qRound(x*100.0)/100.0,'f',2));
    }
  //    toReplot=true;
  //customPlot->replot();
  //        toReplot=false;


  cursorEnabled=true;

}

QVector<double> ImageClass::MeanRowHSV(ImageClass *Image, int row)
//obliczenie średniej wartości HSV pikseli w danym wierszu ROW
//zwraca vector 3 elementów: 0 - hue, 1 saturation, 2 - value, średnie HSV
{
  QVector<double> temp(3),meanVec(3);
  meanVec[0]=0;
  meanVec[1]=0;
  meanVec[2]=0;
  double hVecDivider=0;

  for (int i=0; i<Image->fotoHSV.cols; i++)
    {
      Vec3b intensity = Image->fotoHSV.at<Vec3b>(row, i);
      temp[0] = (double)intensity.val[0]; //HUE - wartości 0..180
      temp[1] = (double)intensity.val[1]; //Saturation - wartości 0..255
      temp[2] = (double)intensity.val[2]; //Value - wartości 0..255

      //normalizacja i konwersja na zakresy H:0..360, S:0..100, V:0..100
      temp[0]*=2;
      temp[1]/=2.55;
      temp[2]/=2.55;
      //        temp[0]*=360.0;
      //        temp[1]*=100.0;
      //        temp[2]*=100.0;

      //filtracja koloru dla H>250 pomiń próbki
      if (temp[0]<250)
        {
          meanVec[0] += temp[0];
          meanVec[1] += temp[1];
          meanVec[2] += temp[2];
          hVecDivider++;
        }


    }

  meanVec[0] /=hVecDivider;
  meanVec[1] /=hVecDivider;
  meanVec[2] /=hVecDivider;
  return meanVec;
}

int MainWindow::setGradient(QWidget *parent)
//ustawia wartości osi X danych obiektu DataClass na podstawie dobranego ręcznie gradientu
{

  double min=ui->lineEditMIN->value();
  double max=ui->lineEditMAX->value();
  if (Config->status)
    {
      if (max>min)
        {
          QModbusServer::State ns=modbusDevice->state();
          if (ns==QModbusDevice::ConnectedState)//2-połączono z siecią modbus, 0-brak połączenia
            {
              QVector<double> emptyVec;
              Config->min=emptyVec;
              Config->max=emptyVec;
              Config->min.append(min);
              Config->max.append(max);

            }
          else
            {
              QMessageBox::warning(parent,"Brak połączenia",
                                   "Nie połączono z PLC. Sprawdź połączenie i spróbuj ponownie");
              return -2;
            }


          return 0;
        }
      else
        {
          QMessageBox::warning(parent,"Błędny gradient",
                               "Wprowadź prawidłowo gradient MAX>MIN");
          return -1;
        }
    }
  else
    {
      QMessageBox::warning(this, "Brak konfiguracji",
                           "<p>Należy skonfigurować program przed rozpoczęciem pracy.</p> ");
      return -1;
    }

}

int MainWindow::SaveToCSV(QWidget *parent, DataClass *_Data, QString _fileName)
//zapisze dane do pliku pod warunkiem, że obiekt DataClass zawiera
//pełny zestaw danych (X, i trzy Y - HSV)
//zwraca -1 gdy nie wybrano pliku oraz -2 gdy dane są nieprawidłowe
{
  QVector<double> tempX, tempH, tempS, tempV;
  if (_fileName=="")
    {
      if (_Data->isData)
        {
          tempX = _Data->xTemp;
          tempH = _Data->yH;
          tempS = _Data->yS;
          tempV = _Data->yV;

          QString fileName = QFileDialog::getSaveFileName(parent,QFileDialog::tr("Zapisz do CSV"),
                                                          QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
          QFile data(fileName);
          if(data.open(QFile::WriteOnly |QFile::Truncate))
            {
              QTextStream output(&data);

              output <<  "Temperature" << ";" << "HUE" << ";" << "Saturation" << ";" << "Value"<<"\n";
              for (int i=0; i<(_Data->xTemp.size()); i++)
                {
                  output <<tempX[i]<< ";" << tempH[i] << ";"<< tempS[i]<<";"<<tempV[i]<<"\n";
                }
              data.close();
              return 0;
            }
          else
            return -1; //nie wybrano pliku

        }
      else
        return -2; //nieprawidłowe dane

    }
  else
    //funkcja wywołana z argumentem fileName
    {
      tempX = _Data->xTemp;
      tempH = _Data->yH;
      tempS = _Data->yS;
      tempV = _Data->yV;

      QFile data(_fileName);
      if(data.open(QFile::WriteOnly |QFile::Truncate))
        {
          QTextStream output(&data);

          output <<  "Temperature" << ";" << "HUE" << ";" << "Saturation" << ";" << "Value"<<"\n";
          for (int i=0; i<(_Data->xTemp.size()); i++)
            {
              output <<tempX[i]<< ";" << tempH[i] << ";"<< tempS[i]<<";"<<tempV[i]<<"\n";
            }
          data.close();
          return 0;
        }
      else
        return -1; //nie wybrano pliku
    }

}


int MainWindow::OpenCSV(QWidget *parent, DataClass *_Data, QString _fileName)
//otwarcie pliku CSV danych z dysku
{
  QString temp;
  QStringList temp2;
  double value0, value1, value2, value3;
  QVector<double> value0v,value1v, value2v, value3v;
  if (_fileName=="")
    {

      QString fileName = QFileDialog::getOpenFileName(
            parent,
            QFileDialog::tr("Otwórz plik CSV"),
            QDir::currentPath()+"/FILES/",
            QFileDialog::tr("Plik CSV (*.csv)"));
      if (!fileName.isNull())
        {
          _Data->name=fileName.section("/",-1,-1);
          _Data->path=fileName.section("/",0,-2);
          QFile data(fileName);
          if (data.open(QFile::ReadOnly))
            {
              QTextStream input(&data);
              int i=0;
              while (!input.atEnd())
                {
                  temp=input.readLine();
                  temp2=temp.split(';');
                  if (i>0)
                    {
                      value0 = QString(temp2[0]).toDouble();
                      value1 = QString(temp2[1]).toDouble();
                      value2 = QString(temp2[2]).toDouble();
                      value3 = QString(temp2[3]).toDouble();

                      value0v += value0;
                      value1v += value1;
                      value2v += value2;
                      value3v += value3;

                    }

                  i++;
                }
              _Data->xTemp=value0v;
              _Data->yH=value1v;
              _Data->yS=value2v;
              _Data->yV=value3v;
              _Data->isData=true;

              return 0;
            }
          else
            {
              QMessageBox::warning(parent, "Błąd", "Nie można otworzyć pliku.");
              return -1;
            }

        }
      else
        {
          QMessageBox::warning(parent, "Nie wybrano pliku", "Należy wybrać prawidłowy plik do otwarcia.");
          return -2;
        }
    }
  else
    //funkcja wywołana ze stałą ścieżką do pliku
    {

      if (!_fileName.isNull())
        {
          _Data->name=_fileName.section("/",-1,-1);
          _Data->path=_fileName.section("/",0,-2);
          QFile data(_fileName);
          if (data.open(QFile::ReadOnly))
            {
              QTextStream input(&data);
              int i=0;
              while (!input.atEnd())
                {
                  temp=input.readLine();
                  temp2=temp.split(';');
                  if (i>0)
                    {
                      value0 = QString(temp2[0]).toDouble();
                      value1 = QString(temp2[1]).toDouble();
                      value2 = QString(temp2[2]).toDouble();
                      value3 = QString(temp2[3]).toDouble();

                      value0v += value0;
                      value1v += value1;
                      value2v += value2;
                      value3v += value3;

                    }

                  i++;
                }
              _Data->xTemp=value0v;
              _Data->yH=value1v;
              _Data->yS=value2v;
              _Data->yV=value3v;
              _Data->isData=true;

              return 0;
            }
          else
            {
              QMessageBox::warning(parent, "Błąd", "Nie można otworzyć pliku.");
              return -1;
            }

        }
      QMessageBox::warning(parent, "Błąd", "Nie można otworzyć pliku.");
      return -1;
    }

}

void MainWindow::autoScale(QCustomPlot *customPlot, DataClass *Data, DataClass *Reference)
{
  double minX=30,maxX=40;
  if (Data->isData)
    {
      //ustaw mx osi X danych
      maxX=Data->xTemp[Data->xTemp.size()-1];

      //ustaw min osi X
      minX=Data->xTemp[0];
    }
  else if (Reference->isData)
    {
      maxX=Reference->xTemp[Reference->xTemp.size()-1];

      minX=Reference->xTemp[0];
    }

  if (Reference->isData)
    {
      //ustaw max osi X referencji
      if ((Reference->xTemp[Reference->xTemp.size()-1])>maxX)
        maxX=Reference->xTemp[Reference->xTemp.size()-1];

      //ustaw min osi X
      if (Reference->xTemp[0]<minX)
        minX=Reference->xTemp[0];

    }
  //ustaw zakres skali X i Y
  customPlot->yAxis->setRange(0,420);
  customPlot->xAxis->setRange(minX,maxX);

}

void MainWindow::print(QCustomPlot *customPlot, DataClass *_Data, int plotNo)
//plotNo==1 - zwykły wykres, plotNo==2 - wykres referencyjny

{
  if (Data->isData||Reference->isData)
    {

      customPlot->legend->setVisible(true);
      if(plotNo==1)
        //rysowanie wykresu zwykłych danych
        {
          //data graph 1
          customPlot->graph(0)->setData(_Data->xTemp, _Data->yH);
          customPlot->graph(0)->setPen(QPen(Qt::red, 2));
          customPlot->graph(0)->setName(_Data->name+ " HUE");
          //data graph 2
          customPlot->graph(1)->setData(_Data->xTemp, _Data->yS);
          customPlot->graph(1)->setPen(QPen(Qt::darkGreen,2));
          customPlot->graph(1)->setName(_Data->name+" Saturation");

          //data graph 3
          customPlot->graph(2)->setData(_Data->xTemp, _Data->yV);
          customPlot->graph(2)->setPen(QPen(Qt::black,2));
          customPlot->graph(2)->setName(_Data->name+" Value");

          // ustawienie skali osi
          autoScale(customPlot,Data,Reference);
          customPlot->replot();
          ui->listWidget->addItem(_Data->path + "/" + _Data->name);



        }
      else if (plotNo==2)
        //rysowanie wykresu referencyjnego
        {

          //reference graph 1
          customPlot->graph(3)->setData(_Data->xTemp, _Data->yH);
          customPlot->graph(3)->setPen(QPen(Qt::red,2, Qt::DashLine));
          customPlot->graph(3)->setName(_Data->name+ " HUE");

          //reference graph 2
          customPlot->graph(4)->setData(_Data->xTemp, _Data->yS);
          customPlot->graph(4)->setPen(QPen(Qt::darkGreen, 2, Qt::DashLine));
          customPlot->graph(4)->setName(_Data->name+" Saturation");

          //reference graph 3
          customPlot->graph(5)->setData(_Data->xTemp, _Data->yV);
          customPlot->graph(5)->setPen(QPen(Qt::black,2, Qt::DashLine));
          customPlot->graph(5)->setName(_Data->name+" Value");

          // skalowanie osi
          autoScale(customPlot,Data,Reference);
          customPlot->replot();
          ui->listWidget->addItem(_Data->path + "/" +_Data->name);


        }
    }
  calculateTempErrors();

}

double MainWindow::calculateParameter(QVector<double> yData, QVector<double> xData, double key)
{
    double delta = 100000000.0;
    double parameterX = xData.at(0);
    double previousY;
    double nextY;

    for(int i=10; i < xData.size() - 10; i++){
        nextY = yData.at(i+10);
        previousY = yData.at(i-10);

        if(nextY > previousY){
            if(qFabs(key - yData.at(i)) < delta){
                delta = qFabs(key - yData.at(i));
                parameterX = xData.at(i);
            }
        }

    }
    return parameterX;
}

void MainWindow::calculateTempErrors()
{
  double refTr, refTg, refTh, refTb, pomTr, pomTg, pomTh, pomTb, dTr, dTg, dTh, dTb;

  if(Data->isData){
      //wyznaczanie temperaturowej odpowiedzi barwowej dla danych Data
      wykresTemp->graph(0)->setData(Data->yH,Data->xTemp);
      wykresTemp->graph(0)->setVisible(false);
      dataMap6=wykresTemp->graph(0)->data();

      wykresTemp->graph(1)->setData(Data->yS,Data->xTemp);
      wykresTemp->graph(1)->setVisible(false);
      dataMap7=wykresTemp->graph(1)->data();

      pomTr = calculateParameter(Data->yH, Data->xTemp, Config->H_Tr);
      ui->labelPOMTr->setText(QString::number(qRound(pomTr*1000.0)/1000.0,'f',3));

      pomTg = calculateParameter(Data->yH, Data->xTemp, Config->H_Tg);
      ui->labelPOMTg->setText(QString::number(qRound(pomTg*1000.0)/1000.0,'f',3));

      pomTb = calculateParameter(Data->yH, Data->xTemp, Config->H_Tb);
      ui->labelPOMTb->setText(QString::number(qRound(pomTb*1000.0)/1000.0,'f',3));

      pomTh = calculateParameter(Data->yH, Data->xTemp, Config->H_Th);
      ui->labelPOMTh->setText(QString::number(qRound(pomTh*1000.0)/1000.0,'f',3));

    }
  else{//Data->isData==false
      ui->labelPOMTr->setText("PomTr");
      ui->labelPOMTg->setText("PomTg");
      ui->labelPOMTh->setText("PomTh");
      ui->labelPOMTb->setText("PomTb");
    }

  if (Reference->isData){
      //wyznaczanie temperaturowej odpowiedzi barwowej dla danych Reference
      wykresTemp->graph(2)->setData(Reference->yH,Reference->xTemp);
      wykresTemp->graph(2)->setVisible(false);
      dataMap8=wykresTemp->graph(2)->data();

      wykresTemp->graph(3)->setData(Reference->yS,Reference->xTemp);
      wykresTemp->graph(3)->setVisible(false);
      dataMap9=wykresTemp->graph(3)->data();

      refTr = calculateParameter(Reference->yH,Reference->xTemp, Config->H_Tr);
      ui->labelREFTr->setText(QString::number(qRound(refTr*1000.0)/1000.0,'f',3));


      refTg = calculateParameter(Reference->yH,Reference->xTemp, Config->H_Tg);
      ui->labelREFTg->setText(QString::number(qRound(refTg*1000.0)/1000.0,'f',3));


      refTh = calculateParameter(Reference->yH,Reference->xTemp, Config->H_Th);
      ui->labelREFTh->setText(QString::number(qRound(refTh*1000.0)/1000.0,'f',3));


      refTb = calculateParameter(Reference->yH,Reference->xTemp, Config->H_Tb);
      ui->labelREFTb->setText(QString::number(qRound(refTb*1000.0)/1000.0,'f',3));

    }
  else{//Reference->isData==false
      ui->labelREFTr->setText("refTr");
      ui->labelREFTg->setText("RefTg");
      ui->labelREFTh->setText("RefTh");
      ui->labelREFTb->setText("RefTb");
    }

  if (Data->isData&&Reference->isData)
    //obliczenie bledow temperaturowych dla referencji i danych
    {
      ui->reportButton->setEnabled(true);
      dTr=refTr-pomTr;
      dTg=refTg-pomTg;
      dTh=refTh-pomTh;
      dTb=refTb-pomTb;

      Config->dTr=dTr;
      Config->dTg=dTg;
      Config->dTh=dTh;
      Config->dTb=dTb;

      Config->refTr=refTr;
      Config->refTg=refTg;
      Config->refTh=refTh;
      Config->refTb=refTb;

      Config->pomTr=pomTr;
      Config->pomTg=pomTg;
      Config->pomTh=pomTh;
      Config->pomTb=pomTb;

      //      ui->labelBLADTR->setText(QString::number(qRound(dTr*1000.0)/1000.0,'f',3)+" [ᴼC]");
      //      ui->labelBLADTG->setText(QString::number(qRound(dTg*1000.0)/1000.0,'f',3)+" [ᴼC]");
      //      ui->labelBLADTH->setText(QString::number(qRound(dTh*1000.0)/1000.0,'f',3)+" [ᴼC]");
      //      ui->labelBLADTB->setText(QString::number(qRound(dTb*1000.0)/1000.0,'f',3)+" [ᴼC]");

      //ustawienie kolorów czcionki błędów Tr
      if (qFabs(dTr)>Config->HD_Tr_H&&qFabs(dTr)<Config->HD_Tr_HH){
          ui->labelBLADTR->setText("<font color=orange>"
                                   +QString::number(qRound(dTr*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTrError=1;
        }
      else if (qFabs(dTr)>Config->HD_Tr_HH){
          ui->labelBLADTR->setText("<font color=red>"
                                   +QString::number(qRound(dTr*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTrError=2;
        }
      else{
          ui->labelBLADTR->setText("<font color=green>"
                                   +QString::number(qRound(dTr*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTrError=0;
        }
      //ustawienie kolorów czcionki błędów Tg
      if (qFabs(dTg)>Config->HD_Tg_H&&qFabs(dTg)<Config->HD_Tg_HH){
          ui->labelBLADTG->setText("<font color=orange>"
                                   +QString::number(qRound(dTg*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTgError=1;
        }
      else if (qFabs(dTg)>Config->HD_Tg_HH){
          ui->labelBLADTG->setText("<font color=red>"
                                   +QString::number(qRound(dTg*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTgError=2;
        }
      else{
          ui->labelBLADTG->setText("<font color=green>"
                                   +QString::number(qRound(dTg*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTgError=0;
        }
      //ustawienie kolorów czcionki błędów Th
      if (qFabs(dTh)>Config->HD_Th_H&&qFabs(dTh)<Config->HD_Th_HH){
          ui->labelBLADTH->setText("<font color=orange>"
                                   +QString::number(qRound(dTh*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dThError=1;
        }
      else if (qFabs(dTh)>Config->HD_Th_HH){
          ui->labelBLADTH->setText("<font color=red>"
                                   +QString::number(qRound(dTh*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dThError=2;
        }
      else{
          ui->labelBLADTH->setText("<font color=green>"
                                   +QString::number(qRound(dTh*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dThError=0;
        }
      //ustawienie kolorów czcionki błędów Tb
      if (qFabs(dTb)>Config->HD_Tb_H&&qFabs(dTb)<Config->HD_Tb_HH){
          ui->labelBLADTB->setText("<font color=orange>"
                                   +QString::number(qRound(dTb*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTbError=1;
        }
      else if (qFabs(dTb)>Config->HD_Tb_HH){
          ui->labelBLADTB->setText("<font color=red>"
                                   +QString::number(qRound(dTb*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTbError=2;
        }
      else{
          ui->labelBLADTB->setText("<font color=green>"
                                   +QString::number(qRound(dTb*1000.0)/1000.0,'f',3)+" [ᴼC]</font>");
          Config->dTbError=0;
        }

      //kod umożliwiający zrobienie ramki - background z kolorem

      //    ui->labelBLADTR->setStyleSheet("QLabel { background: rgb(0,200,0); color : black; "
      //                                   "border-style: outset;"
      //                                   "border-width: 1px;"
      //                                   "border-radius: 5px;"
      //                                   "border-color: rgb(0,160,0);}");
      //    ui->labelBLADTG->setStyleSheet("QLabel { background: rgb(200,200,0); color : black; "
      //                                   "border-style: outset;"
      //                                   "border-width: 1px;"
      //                                   "border-radius: 5px;"
      //                                   "border-color: rgb(160,160,0);}");
      //    ui->labelBLADTH->setStyleSheet("QLabel { background: rgb(180,0,0); color : black; "
      //                                   "border-style: outset;"
      //                                   "border-width: 1px;"
      //                                   "border-radius: 5px;"
      //                                   "border-color: rgb(140,0,0);}");

      if (Config->dTrError==0&&Config->dTgError==0&&Config->dThError==0&&Config->dTbError==0)
        Config->result="POZYTYWNY";
      else if(Config->dTrError==2||Config->dTgError==2||Config->dThError==2||Config->dTbError==2)
        Config->result="NEGATYWNY";
      else
        Config->result="OSTRZEZENIE";
    }
  else{
      ui->labelBLADTR->setText("<font color=black>bladTr</font>");
      ui->labelBLADTG->setText("<font color=black>bladTg</font>");
      ui->labelBLADTH->setText("<font color=black>bladTh</font>");
      ui->labelBLADTB->setText("<font color=black>bladTb</font>");
      ui->reportButton->setEnabled(false);

    }
}
void MainWindow::initializeGraph(QCustomPlot *customPlot,QCustomPlot *_temp)
{
  customPlot->clearGraphs();
  customPlot->installEventFilter(this);
  // latin UNICODE character "ᴼ"<-naleYz skopiować i wstawić w QStringu

  QFont labelFont = font();  // start out with MainWindow's font..
  labelFont.setPointSize(11);
  customPlot->xAxis->setLabelFont(labelFont);
  customPlot->xAxis->setLabel("Temperatura [ᴼC]");
  customPlot->yAxis->setLabelFont(labelFont);
  customPlot->yAxis->setLabel("Średnia HSV");
  // ustawienie skali osi
  customPlot->xAxis->setRange(30, 40);
  customPlot->yAxis->setRange(0, 420);
  customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
  customPlot->legend->setVisible(false);


  for (int i=0; i<GRAPH_COUNT; i++)
    //utworzenie nazw pustych wykresów
    {
      customPlot->addGraph();
      customPlot->graph(i)->setName("");
    }
  customPlot->replot();
  _temp->clearGraphs();
  for (int i=0;i<4;i++)
    {
      _temp->addGraph();
      _temp->graph(i)->setName("");
    }
  _temp->setVisible(false);
  calculateTempErrors();
  customPlot->setNoAntialiasingOnDrag(true);

}

void MainWindow::on_openMat_clicked()
{
  //otwarcie grafiki z pliku

  if (Image1->OpenMatFile(this,Image1,Data)==0)
    {

      ui->gradientButton->setEnabled(true);
      Mat source, source2, tempImg1,tempImg2;
      cv::Mat tmpMap1;
      source=Image1->fotoBGR;
      source2=Image1->fotoHSV;
      transpose(source,tempImg1);
      transpose(source2,tempImg2);
      cv::resize(tempImg1, tempImg1, Size(ui->imgLabelRGB->width(),ui->imgLabelRGB->width()), 0, 0, INTER_LINEAR);
      cv::resize(tempImg2, tempImg2, Size(ui->imgLabelHSV->width(),ui->imgLabelHSV->width()), 0, 0, INTER_LINEAR);
      QImage qImg1 = Mat2QImage(tempImg1);
      QImage qImg2 = Mat2QImage(tempImg2);
      ui->imgLabelRGB->setPixmap(QPixmap::fromImage(qImg1));
      ui->imgLabelHSV->setPixmap(QPixmap::fromImage(qImg2));

    }
  else
    {
      //można wyświetlić komunikat
    }
}

int ImageClass::OpenMatFile(QWidget *parent, ImageClass *Image, DataClass *Data)
//funkcja otwierająca obraz z pliku i konwertująca na MAT
{
  QString fileName = QFileDialog::getOpenFileName(parent,
                                                  QFileDialog::tr("Otwórz plik obrazu"),
                                                  QDir::currentPath()+"/FILES/",
                                                  QFileDialog::tr("Pliki obrazów (*.png *.jpg *.bmp)"));
  if (!fileName.isNull())
    {
      Image->fotoBGR = cv::imread(fileName.toStdString());
      Data->name="Pobrany obraz";
      Data->path="";
      //konwersja na HSV
      cvtColor(Image->fotoBGR, Image->fotoHSV,CV_BGR2HSV);
      QVector<double> temp(3),tmpH(Image->fotoHSV.rows), tmpS(Image->fotoHSV.rows), tmpV(Image->fotoHSV.rows), tmpX(Image->fotoHSV.rows);

      for (int j=0; j<Image->fotoHSV.rows; j++)
        {
          temp=Image->MeanRowHSV(Image,j);
          tmpH[j]=temp[0];
          tmpS[j]=temp[1];
          tmpV[j]=temp[2];
        }
      //przypisanie wartości Y do obiektu typu DataClass
      Data->yH=tmpH;
      Data->yS=tmpS;
      Data->yV=tmpV;

      return 0;
    }
  else
    {
      QMessageBox::warning(parent, "Nie wybrano pliku", "Należy wybrać prawidłowy plik do otwarcia.");
      return -1;
    }

}

void MainWindow::getImage()
//Pobierz obraz z kamery
{
  // setRegister(1,4);//włącz światło
  mThread->setConfig(Config->ID_Cam,Config->map1,Config->map2,
                     Config->lightCalB,Config->lightCalG,Config->lightCalR, Config->lightCalOption);
  mThread->start();


}


void MainWindow::on_captureImg_clicked()
{
  Config->plcRegisters[4]=1;

  //  setRegister(1,4); //wyślij polecenie włączenia światła
  states->currentState=1;
}
bool MainWindow::captureCalibrate()
{

  if (Image1->fotoBGR.cols>0)
    {
      Mat source, source2, tempImg1,tempImg2;
      source=Image1->fotoBGR;
      source2=Image1->fotoHSV;
      cv::transpose(Image1->fotoBGR,tempImg1);
      cv::transpose(Image1->fotoHSV,tempImg2);
      cv::resize(tempImg1, tempImg1, Size(1500,30), 0, 0, INTER_LINEAR);
      cv::resize(tempImg2, tempImg2, Size(1500,30), 0, 0, INTER_LINEAR);
      QImage qImg1 = Mat2QImage(tempImg1);
      QImage qImg2 = Mat2QImage(tempImg2);
      ui->imgLabelRGB->setPixmap(QPixmap::fromImage(qImg1));
      ui->imgLabelHSV->setPixmap(QPixmap::fromImage(qImg2));

      QVector<double> tmp=Data->xTemp;
      Reference->xTemp.append(tmp);
      tmp=Data->yH;
      Reference->yH.append(tmp);
      tmp=Data->yS;
      Reference->yS.append(tmp);
      tmp=Data->yV;
      Reference->yV.append(tmp);
      tmp=Reference->yH;

      //sort(Reference,0,Reference->xTemp.size()-1);
      Reference->isData=true;
      Reference->path="";
      Reference->name="Referencja";

      sort(Data,0,Data->xTemp.size()-1);
      MeanData(Data,10);
      print(ui->wykres,Data,1);
      Data->isData=false;


      Config->plcRegisters[4]=0;
      //          setRegister(0,4);
      emit changeState();
      return true;
    }
  else
    {
      Config->plcRegisters[4]=0;
      //          setRegister(0,4);//wyłącz światło jeśli brak połączenia z kamerą
      QMessageBox::information(this,"Brak połączenia z kamerą",
                               "Brak połączenia z kamerą lub kamera zajęta");
      ui->calibrateButton->setEnabled(true);
      emit changeStateToInit();
      return false;
    }
}

void MainWindow::captureImg(bool sortMean)
{

  if (Image1->fotoBGR.cols>0)
    {

      if (sortMean)
        {
          sort(Data,0,Data->xTemp.size()-1);
          MeanData(Data,10);
        }

      ui->gradientButton->setEnabled(true);
      Mat source, source2, tempImg1,tempImg2;
      source=Image1->fotoBGR;
      source2=Image1->fotoHSV;
      cv::transpose(Image1->fotoBGR,tempImg1);
      cv::transpose(Image1->fotoHSV,tempImg2);
      cv::resize(tempImg1, tempImg1, Size(1500,30), 0, 0, INTER_LINEAR);
      cv::resize(tempImg2, tempImg2, Size(1500,30), 0, 0, INTER_LINEAR);
      //   imshow("BGR",Image1->fotoBGR);
      QImage qImg1 = Mat2QImage(tempImg1);
      QImage qImg2 = Mat2QImage(tempImg2);
      ui->imgLabelRGB->setPixmap(QPixmap::fromImage(qImg1));
      ui->imgLabelHSV->setPixmap(QPixmap::fromImage(qImg2));
      ui->SaveFileButton->setEnabled(true);
      print(ui->wykres,Data,1);
      Config->plcRegisters[4]=0;
      //          setRegister(0,4);
      emit changeState();
    }
  else
    {
      Config->plcRegisters[4]=0;
      //          setRegister(0,4); //wyślij polecenie włączenia światła
      QMessageBox::information(this,"Brak połączenia z kamerą",
                               "Brak połączenia z kamerą lub kamera zajęta");
      ui->gradientButton->setEnabled(true);
      emit changeStateToInit();
    }

}


void MainWindow::on_OpenFileButton_clicked()
{
  if (OpenCSV(this,Data)==0)
    {

      //ui->OpenFileButton->setEnabled(false);
      MainWindow::print(ui->wykres,Data,1);
    }
}

void MainWindow::setDefaultGradients(int refPathNo)
{
    double minValue = 20.0;
    double maxValue = 20.0;
    switch (refPathNo) {
    case 0:
        minValue = Config->Ref_I_MIN;
        maxValue = Config->Ref_I_MAX;
        break;
    case 1:
        minValue = Config->Ref_Ia_MIN;
        maxValue = Config->Ref_Ia_MAX;
        break;
    case 2:
        minValue = Config->Ref_II_MIN;
        maxValue = Config->Ref_II_MAX;
        break;
    default:
        break;
    }
    ui->lineEditMIN->setValue(minValue);
    ui->lineEditMAX->setValue(maxValue);
}

void MainWindow::on_OpenReference_clicked()
{
  int refPathNo=0;
  ReferenceDialog newRef(this);
  newRef.setModal(true);
  newRef.exec();
  if (newRef.getStatus())
    {
      refPathNo=newRef.getRefPathNo();

      if (OpenCSV(this,Reference,Config->refPaths[refPathNo])==0)
        {
          setDefaultGradients(refPathNo);
          ui->OpenReference->setEnabled(false);
          ui->gradientButton->setEnabled(true);
          MainWindow::print(ui->wykres,Reference,2);
        }
    }

}

void MainWindow::on_SaveFileButton_clicked()
{
  if (states->screen==1)
    {
      SaveToCSV(this,Data);
    }
  else if(states->screen=2)
    {
      SaveToCSV(this,Reference);
    }
}

void MainWindow::on_actionAbout_triggered()
{
  About newAbout;
  newAbout.setModal(true);
  newAbout.exec();
}

void MainWindow::on_actionZakoncz_triggered()
{
  if (QMessageBox::question(this,"Zakończyć","Czy na pewno chcesz zakończyć pracę?",QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
    {
      QApplication::quit();
    }

}

void MainWindow::on_actionAnaliza_triggered()
{
  //  if(Data->isData||Reference->isData||states->currentState>0)
  //    {

  if (QMessageBox::question(this,"Potwierdź","Czy na pewno chcesz przełączyć tryb pracy?",
                            QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
    {
      Data->isData=false;
      Reference->isData=false;

      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      setVisible("Analiza");
      states->currentState=0;
      Config->plcRegisters[1]=0;
      Config->plcRegisters[2]=0;
      Config->plcRegisters[3]=0;
      Config->plcRegisters[4]=0;
      //          setRegister(0,1);
      //          setRegister(0,2);
      //          setRegister(0,3);
    }
  else
    {

    }

  //    }
  //  else
  //    {
  //      initializeGraph(ui->wykres);
  //      ui->listWidget->clear();
  //      setVisible("Analiza");
  //      setRegister(0,1);
  //      setRegister(0,2);
  //      setRegister(0,3);
  //    }

}

void MainWindow::on_actionKalibracja_triggered()
{
  //  if(Data->isData||Reference->isData||states->currentState>0)
  //    {

  if (QMessageBox::question(this,"Potwierdź","Czy na pewno chcesz przełączyć tryb pracy?",QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
    {
      Data->isData=false;
      Reference->isData=false;

      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      setVisible("Kalibracja");
      states->currentState=0;
      Config->plcRegisters[1]=0;
      Config->plcRegisters[2]=0;
      Config->plcRegisters[3]=0;
      Config->plcRegisters[4]=0;
      //          setRegister(0,1);
      //          setRegister(0,2);
      //          setRegister(0,3);
      ui->calibrateButton->setEnabled(true);
    }
  else
    {

    }
}

void MainWindow::on_actionTest_triggered()
{

  if (QMessageBox::question(this,"Potwierdź","Czy na pewno chcesz przełączyć tryb pracy?",QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
    {
      Data->isData=false;
      Reference->isData=false;

      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      setVisible("Test");
      states->currentState=0;
      Config->plcRegisters[1]=0;
      Config->plcRegisters[2]=0;
      Config->plcRegisters[3]=0;
      Config->plcRegisters[4]=0;
      //          setRegister(0,1);
      //          setRegister(0,2);
      //          setRegister(0,3);
    }
  else
    {

    }
}

void MainWindow::on_plusButton_2_clicked()
{
  //powiększ po osi X
  double newXl=(ui->wykres->xAxis->range().lower)+0.1*((ui->wykres->xAxis->range().upper)-(ui->wykres->xAxis->range().lower));
  double newXu=(ui->wykres->xAxis->range().upper)-0.1*((ui->wykres->xAxis->range().upper)-(ui->wykres->xAxis->range().lower));
  ui->wykres->xAxis->setRange(newXl,newXu);
  //powiększ po osi Y
  newXl=(ui->wykres->yAxis->range().lower)+0.1*((ui->wykres->yAxis->range().upper)-(ui->wykres->yAxis->range().lower));
  newXu=(ui->wykres->yAxis->range().upper)-0.1*((ui->wykres->yAxis->range().upper)-(ui->wykres->yAxis->range().lower));
  ui->wykres->yAxis->setRange(newXl,newXu);

  ui->wykres->replot();

}

void MainWindow::on_minusButton_2_clicked()
{
  double newXl=(ui->wykres->xAxis->range().lower)-0.1*((ui->wykres->xAxis->range().upper)-(ui->wykres->xAxis->range().lower));
  double newXu=(ui->wykres->xAxis->range().upper)+0.1*((ui->wykres->xAxis->range().upper)-(ui->wykres->xAxis->range().lower));
  ui->wykres->xAxis->setRange(newXl,newXu);

  newXl=(ui->wykres->yAxis->range().lower)-0.1*((ui->wykres->yAxis->range().upper)-(ui->wykres->yAxis->range().lower));
  newXu=(ui->wykres->yAxis->range().upper)+0.1*((ui->wykres->yAxis->range().upper)-(ui->wykres->yAxis->range().lower));
  ui->wykres->yAxis->setRange(newXl,newXu);
  ui->wykres->replot();
}

void MainWindow::on_zeroButton_2_clicked()
{
  autoScale(ui->wykres,Data,Reference);
  //ui->wykres->rescaleAxes();
  ui->wykres->replot();
}

void MainWindow::on_zastapButton_clicked()
{
  if (ui->comboBox->currentIndex()==0)
    {
      if (OpenCSV(this,Reference)==0)
        {
          print(ui->wykres,Reference,2);
        }
    }
  else if (ui->comboBox->currentIndex()==1)
    {
      if (OpenCSV(this,Data)==0)
        {
          print(ui->wykres,Data,1);
        }
    }

}

void MainWindow::on_wyczyscButton_clicked()
{
  if (QMessageBox::question(this,"Potwierdź","Czy na pewno chcesz wyczyścić wykres?",
                            QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
    {
      Data->isData=false;
      Reference->isData=false;
      Data->clear();
      Reference->clear();

      initializeGraph(ui->wykres,wykresTemp);
      ui->listWidget->clear();
      ui->OpenFileButton->setEnabled(true);
      ui->OpenReference->setEnabled(true);
      ui->gradientButton->setEnabled(false);
      ui->captureImg->setEnabled(false);
      ui->gradientButton->setEnabled(true);
      ui->SaveFileButton->setEnabled(false);
      ui->calibrateButton->setEnabled(true);

      ui->imgLabelRGB->clear();
      ui->imgLabelHSV->clear();
      ui->labelPOMH->setText("pomH");
      ui->labelPOMS->setText("pomS");
      ui->labelPOMV->setText("pomV");
      ui->labelREFH->setText("refH");
      ui->labelREFS->setText("refS");
      ui->labelREFV->setText("refV");

      ui->labelBLADH->setText("bladH");
      ui->labelBLADS->setText("bladS");
      ui->labelBLADV->setText("bladV");

      ui->labelBLADTR->setText("<font color=black>bladTr</font>");
      ui->labelBLADTG->setText("<font color=black>bladTg</font>");
      ui->labelBLADTH->setText("<font color=black>bladTh</font>");
      ui->labelBLADTB->setText("<font color=black>bladTb</font>");

      setupDeviceData();
      emit changeStateToInit();
    }

}

void MainWindow::on_actionPodpowiedzi_toggled(bool arg1)
{
  if (arg1)
    ui->wykres->setToolTip("<html><head/><body><p>Wyświetla wartość HSV dla otwartego pliku. "
                           "Kliknij na wykres aby go wybrać.</p></body></html>");

  else
    ui->wykres->setToolTip("");

}

void MainWindow::on_gradientButton_clicked()
{

  if (Config->status&&stateIdle->active())
    {
      QModbusServer::State ns=modbusDevice->state();
      if (ns==QModbusDevice::ConnectedState)//2-połączono z siecią modbus, 0-brak połączenia
        {
          int n=setGradient(this);
          if (n==0)
            {
              Data->clear();
              sampleNoDialog newSampleNo(this);
              newSampleNo.setModal(true);
              newSampleNo.exec();
              if (newSampleNo.getStatus())
                {
                  Config->sampleNo=newSampleNo.getSampleNo();
                  Config->orderNo=newSampleNo.getOrderNo();
                  ui->gradientButton->setEnabled(false);
                  ui->captureImg->setEnabled(false);
                  Config->plcRegisters[1]=1;
                  //                        setRegister(1,1);//praca start - start kalibracji
                  emit changeState();
                }
              else
                QMessageBox::information(this,"Brak numeru próbki","Nie wprowadzono numeru próbki. "
                                                                   "Proces nie zostanie rozpoczęty");

            }

        }
      else
        {
          QMessageBox::warning(this,"Brak połączenia",
                               "Nie połączono z PLC. Sprawdź połączenie i spróbuj ponownie");
        }



    }
  else
    {
      QMessageBox::warning(this, "Brak konfiguracji",
                           "<p>Należy skonfigurować program przed rozpoczęciem pracy.</p> ");
    }
}

void MainWindow::on_checkBoxTEMP_toggled(bool checked)
{
  if (checked)
    ui->lineEditTEMP->setEnabled(false);
  else
    ui->lineEditTEMP->setEnabled(true);
}

void MainWindow::on_lineEditTEMP_valueChanged(double arg1)
{
  //obliczenie wartość HSV dla wartości temperatury w polu lineEditTEMP
  //i wyświetlenie ich w polach wraz z błędami
  //sprawdzenie warunku czy dane i referencja są dostępne
  if (Data->isData&&Reference->isData)
    {
      if (dataMap0!=ui->wykres->graph(0)->data()&&
          dataMap1!= ui->wykres->graph(1)->data()&&
          dataMap2!= ui->wykres->graph(2)->data()&&
          dataMap3!= ui->wykres->graph(3)->data()&&
          dataMap4!= ui->wykres->graph(4)->data()&&
          dataMap5!= ui->wykres->graph(5)->data())
        {
          dataMap0= ui->wykres->graph(0)->data();
          dataMap1= ui->wykres->graph(1)->data();
          dataMap2= ui->wykres->graph(2)->data();
          dataMap3= ui->wykres->graph(3)->data();
          dataMap4= ui->wykres->graph(4)->data();
          dataMap5= ui->wykres->graph(5)->data();
        }


      double x=arg1;
      double pomH, pomS, pomV, refH, refS, refV;
      pomH=dataMap0->lowerBound(x).value().value;
      pomS=dataMap1->lowerBound(x).value().value;
      pomV=dataMap2->lowerBound(x).value().value;
      refH=dataMap3->lowerBound(x).value().value;
      refS=dataMap4->lowerBound(x).value().value;
      refV=dataMap5->lowerBound(x).value().value;

      if ((x>=dataMap0->first().key&&x<=dataMap0->last().key)&&
          (x>=dataMap1->first().key&&x<=dataMap1->last().key)&&
          (x>=dataMap2->first().key&&x<=dataMap2->last().key)&&
          (x>=dataMap3->first().key&&x<=dataMap3->last().key)&&
          (x>=dataMap4->first().key&&x<=dataMap4->last().key)&&
          (x>=dataMap5->first().key&&x<=dataMap5->last().key))
        {

          ui->labelPOMH->setText(QString::number(qRound(pomH*1000.0)/1000.0,'f',3));
          ui->labelPOMS->setText(QString::number(qRound(pomS*1000.0)/1000.0,'f',3));
          ui->labelPOMV->setText(QString::number(qRound(pomV*1000.0)/1000.0,'f',3));
          ui->labelREFH->setText(QString::number(qRound(refH*1000.0)/1000.0,'f',3));
          ui->labelREFS->setText(QString::number(qRound(refS*1000.0)/1000.0,'f',3));
          ui->labelREFV->setText(QString::number(qRound(refV*1000.0)/1000.0,'f',3));

          ui->labelBLADH->setText(QString::number(qRound((refH-pomH)*1000.0)/1000.0,'f',3));
          ui->labelBLADS->setText(QString::number(qRound((refS-pomS)*1000.0)/1000.0,'f',3));
          ui->labelBLADV->setText(QString::number(qRound((refV-pomV)*1000.0)/1000.0,'f',3));

        }
      else
        {
          ui->labelPOMH->setText("pomH");
          ui->labelPOMS->setText("pomS");
          ui->labelPOMV->setText("pomV");
          ui->labelREFH->setText("refH");
          ui->labelREFS->setText("refS");
          ui->labelREFV->setText("refV");

          ui->labelBLADH->setText("bladH");
          ui->labelBLADS->setText("bladS");
          ui->labelBLADV->setText("bladV");


        }
    }

}


void MainWindow::on_actionKonfiguracja_triggered()
//otwarcie nowego okna konfiguracji
{
  Configuration newConfig(this);
  newConfig.setConfigData(Config);
  newConfig.setModal(true);
  newConfig.exec();
  if (newConfig.getStatus()){
      saveConfigFile();

    }


  //  Konfiguracja newKonfig(this,Config->paths,Config->pixels,Config->ID_Cam);
  //    newKonfig.givePaths(Config->paths);
  //    newKonfig.setModal(true);
  //    newKonfig.exec();
  //    if (newKonfig.getStatus())
  //      {
  //        Config->pixels=newKonfig.getValues();
  //        Acq->x=Config->pixels;


  //        if (Config->paths!=newKonfig.getPaths())
  //          {
  //            QVector<cv::Mat> tmp;
  //            tmp=newKonfig.getMaps();
  //            Config->map1=tmp[0];
  //            Config->map2=tmp[1];
  //            Config->paths=newKonfig.getPaths();
  //            Config->status=true;
  //            Config->ID_Cam=newKonfig.getID();
  //           // Config->min=newKonfig.getMin();
  //            //Config->max=newKonfig.getMax();
  //          }

  //      }
  //    else
  //      {

  //      }
}
int MainWindow::partition(DataClass *_Data, int p, int r) // dzielimy tablice na dwie czesci, w pierwszej wszystkie liczby sa mniejsze badz rowne x, w drugiej wieksze lub rowne od x
{
  double x = _Data->xTemp[p]; //tablica[p]; // obieramy x
  int i = p, j = r; // i, j - indeksy w tabeli
  double tmpx,tmpy;
  while (true) // petla nieskonczona - wychodzimy z niej tylko przez return j
    {
      while (_Data->xTemp[j] > x) // dopoki elementy sa wieksze od x
        j--;
      while (_Data->xTemp[i] < x) // dopoki elementy sa mniejsze od x
        i++;
      if (i < j) // zamieniamy miejscami gdy i < j
        {
          tmpx = _Data->xTemp[i];
          _Data->xTemp.replace(i,_Data->xTemp[j]);// tablica[i] = tablica[j];
          _Data->xTemp.replace(j,tmpx);// tablica[j] = w;

          tmpy=_Data->yH[i];
          _Data->yH.replace(i,_Data->yH[j]);// tablica[i] = tablica[j];
          _Data->yH.replace(j,tmpy);// tablica[j] = w;

          tmpy=_Data->yS[i];
          _Data->yS.replace(i,_Data->yS[j]);// tablica[i] = tablica[j];
          _Data->yS.replace(j,tmpy);// tablica[j] = w;

          tmpy=_Data->yV[i];
          _Data->yV.replace(i,_Data->yV[j]);// tablica[i] = tablica[j];
          _Data->yV.replace(j,tmpy);// tablica[j] = w;

          i++;
          j--;
        }
      else{ // gdy i >= j zwracamy j jako punkt podzialu tablicy
          return j;
        }
    }
}

void MainWindow::sort(DataClass *_Data, int p, int r)
{
  int q;
  if (p < r)
    {
      q = partition(_Data,p,r); // dzielimy tablice na dwie czesci; q oznacza punkt podzialu
      sort(_Data, p, q); // wywolujemy rekurencyjnie quicksort dla pierwszej czesci tablicy
      sort(_Data, q+1, r); // wywolujemy rekurencyjnie quicksort dla drugiej czesci tablicy
    }

}

void MainWindow::MeanData(DataClass *_Data, int step)
{
  if (_Data->yH.size()>step)
    {
      QVector<double> dataH(_Data->yH.size()),
          dataS(_Data->yH.size()),
          dataV(_Data->yH.size());
      double tmpH=0,tmpS=0,tmpV=0;
      int j=0;

      for (int i=(0+step/2); i<(_Data->yH.size()-step/2);i++)
        //pętla obliczająca średnią dla wartości "środkowych"
        //(z wyłączeniem step/2 elementów na początku i na końcu)
        {
          tmpH=tmpS=tmpV=0;
          for (j=(i-step/2);j<(i+step/2);j++)
            {

              tmpH+=_Data->yH[j];
              tmpS+=_Data->yS[j];
              tmpV+=_Data->yV[j];
            }
          tmpH=tmpH/step;
          tmpS=tmpS/step;
          tmpV=tmpV/step;
          dataH.replace(i,tmpH);
          dataS.replace(i,tmpS);
          dataV.replace(i,tmpV);
        }
      tmpH=tmpS=tmpV=0;
      for (j=0;j<step/2;j++)
        //pętla obliczająca średnią dla pierwszych step/2 elementów
        {
          tmpH+=_Data->yH[j];
          tmpS+=_Data->yS[j];
          tmpV+=_Data->yV[j];
        }
      tmpH=tmpH/(step/2);
      tmpS=tmpS/(step/2);
      tmpV=tmpV/(step/2);
      for (j=0;j<step/2;j++)
        //przypisanie wartości średniej z step/2 pierwszych próbek do
        //step/2 pierwszych próbek (takie same wartości)
        {
          dataH.replace(j,tmpH);
          dataS.replace(j,tmpS);
          dataV.replace(j,tmpV);
        }
      tmpH=tmpS=tmpV=0;
      for (j=(_Data->yH.size()-step/2);j<_Data->yH.size();j++)
        //pętla obliczająca średnią dla ostatnich step/2 elementów
        {
          tmpH+=_Data->yH[j];
          tmpS+=_Data->yS[j];
          tmpV+=_Data->yV[j];
        }
      tmpH=tmpH/(step/2);
      tmpS=tmpS/(step/2);
      tmpV=tmpV/(step/2);
      for (j=(_Data->yH.size()-step/2);j<_Data->yH.size();j++)
        //przypisanie wartości średniej z step/2 ostatnich próbek do
        //step/2 ostatnich próbek (takie same wartości)
        {
          dataH.replace(j,tmpH);
          dataS.replace(j,tmpS);
          dataV.replace(j,tmpV);
        }
      _Data->yH=dataH;
      _Data->yS=dataS;
      _Data->yV=dataV;
    }

}

void MainWindow::computeYTemp(AcqClass *_Acq, DataClass *_Data)
{
  QVector<double> tmpCoef;
  tmpCoef=_Acq->polyCoef;
  QVector<double> temp(Data->yH.size());
  for (int i=0; i<1600;i++) //1600 - ilosc wierszy ustawiona na stale (2048 minus marginesy)
    {
      temp[i]=(pow(double(i+200),2.0))*tmpCoef[2]+(i+200)*tmpCoef[1]+tmpCoef[0];

    }
  _Data->xTemp=temp;
  _Data->isData=true;

}

void MainWindow::on_calibrateButton_clicked()
{

  if (Config->status&&stateIdle->active())
    {
      QModbusServer::State ns=modbusDevice->state();
      if (ns==QModbusDevice::ConnectedState)//2-połączono z siecią modbus, 0-brak połączenia
        {
          Parameters newParams(this);
          newParams.setModal(true);
          QVector<double> paramsToSet(4);
          paramsToSet[0]=Config->cMin;
          paramsToSet[1]=Config->cMax;
          paramsToSet[2]=Config->cWindow;
          paramsToSet[3]=Config->cStep;
          newParams.setParameters(paramsToSet);
          newParams.exec();
          if (newParams.getStatus()){
              ui->SaveFileButton->setEnabled(false);
              ui->calibrateButton->setEnabled(false);
              QVector<double> paramsTemp(4);
              paramsTemp=newParams.getParameters();
              Config->calcGradientVecs(paramsTemp);
              Reference->clear();
              Data->clear();

              initializeGraph(ui->wykres,wykresTemp);
              ui->listWidget->clear();

              emit changeState();
            }
          else{
              QMessageBox::warning(this,"Brak parametrów", "Wprowadź parametry aby przeprowadzić kalibrację");
            }

          // states->currentState=1;

        }
      else
        {
          QMessageBox::warning(this,"Brak połączenia",
                               "Nie połączono z PLC. Sprawdź połączenie i spróbuj ponownie");
        }
    }
  else
    {
      QMessageBox::warning(this, "Brak konfiguracji",
                           "<p>Należy skonfigurować program przed rozpoczęciem pracy.</p> ");
    }
}

//funkcje obsługujące połączenie MODBUS:

void MainWindow::connectToPLC()
{

  bool intendToConnect = (modbusDevice->state() == QModbusDevice::UnconnectedState);

  statusBar()->clearMessage();

  if (intendToConnect) {

      const QUrl url = QUrl::fromUserInput("192.168.250.220:502");
      modbusDevice->setConnectionParameter(QModbusDevice::NetworkPortParameter, url.port());
      modbusDevice->setConnectionParameter(QModbusDevice::NetworkAddressParameter, url.host());

      modbusDevice->setServerAddress(serverAdd);
      if (!modbusDevice->connectDevice()) {
          statusBar()->showMessage(tr("Brak połączenia: ") + modbusDevice->errorString());
        } else {
          statusBar()->showMessage(tr("Nawiązywanie połączenia z PLC..."));

        }
    } else {
      modbusDevice->disconnectDevice();

    }
}

void MainWindow::initModbus()
{
  modbusDevice = new QModbusTcpServer(this);
  //        if (ui->portEdit->text().isEmpty())
  //          {
  //            ui->serverEdit->setValue(1);
  //            ui->portEdit->setText(QLatin1Literal("192.168.250.220:502"));
  //            ui->serverEdit->setValue(255);
  //          }


  if (!modbusDevice) {

      statusBar()->showMessage(tr("Nie można utworzyć serwera MODBUS."));
    } else {
      QModbusDataUnitMap reg;
      reg.insert(QModbusDataUnit::Coils, { QModbusDataUnit::Coils, 0, 10 });
      reg.insert(QModbusDataUnit::DiscreteInputs, { QModbusDataUnit::DiscreteInputs, 0, 10 });
      reg.insert(QModbusDataUnit::InputRegisters, { QModbusDataUnit::InputRegisters, 0, 50 });
      reg.insert(QModbusDataUnit::HoldingRegisters, { QModbusDataUnit::HoldingRegisters, 0, 50 });

      modbusDevice->setMap(reg);

      connect(modbusDevice, &QModbusServer::dataWritten,
              this, &MainWindow::updateData);
      connect(modbusDevice, &QModbusServer::stateChanged,
              this, &MainWindow::onStateChanged);
      connect(modbusDevice, &QModbusServer::errorOccurred,
              this, &MainWindow::handleDeviceError);

      setupDeviceData();

      //automatyczne połączenie z PLC
      const QUrl url = QUrl::fromUserInput("192.168.250.220:502");
      modbusDevice->setConnectionParameter(QModbusDevice::NetworkPortParameter, url.port());
      modbusDevice->setConnectionParameter(QModbusDevice::NetworkAddressParameter, url.host());

      modbusDevice->setServerAddress(serverAdd); //server address 255
      if (!modbusDevice->connectDevice())
        {
          statusBar()->showMessage(tr("Brak połączenia: ") + modbusDevice->errorString());
        }
      else
        {
          statusBar()->showMessage(tr("Nawiązywanie połączenia z PLC..."));
          timer->start(100);

        }
    }

}

void MainWindow::handleDeviceError(QModbusDevice::Error newError)
{
  if (newError == QModbusDevice::NoError || !modbusDevice)
    return;

  statusBar()->showMessage(modbusDevice->errorString());
}

void MainWindow::onStateChanged(int state)
{

  if (state == QModbusDevice::UnconnectedState)
    {
      //obsługa braku połączenia
    }
  else if (state == QModbusDevice::ConnectedState)
    {
      //obsługa stanu połączenia
    }
}

void MainWindow::updateData(QModbusDataUnit::RegisterType table, int address, int size)
//metoda wywoływana w momencie otrzymania informacji o zmianie rejestrów przez PLC
//(zapis danych przez PLC)
{

  int startRegisterTemp=20;
  QVector<quint16> value(30);
  QVector<double> transfer(12);
  QVector<int> transferState(5);
  int tmp;
  double ambientTemperature;

  if (size>1)
    {
      for (int i = 0; i <(size); ++i) {

          //QString text;
          switch (table) {

            case QModbusDataUnit::HoldingRegisters:
              {
                modbusDevice->data(QModbusDataUnit::HoldingRegisters,i+startRegisterTemp,&value[i]);
                if (i<12) //temperatury odczytywane z PLC
                  {
                    tmp=int(value[i]);
                    double tmp2=double(tmp);
                    tmp2=tmp2/1000.0;
                    transfer[i]=tmp2;
                  }
                else if (i<17) //stany PLC
                  {
                    tmp=int(value[i]);
                    transferState[i-12]=tmp;
                  }
                else if (i==17)
                  {
                    tmp=int(value[i]);
                    double tmp2=double(tmp);
                    tmp2=tmp2/1000.0;
                    ambientTemperature = tmp2;
                  }
              }

              break;
            default:
              break;
            }
          //        bool ok;
          //        quint16 value;
          //        modbusDevice->data(QModbusDataUnit::HoldingRegisters,5,&value);


        }
      ui->label_15->setText(QString::number(transferState[0]));
      ui->label_16->setText(QString::number(transferState[1]));
      ui->label_17->setText(QString::number(transferState[2]));
      ui->label_18->setText(QString::number(transferState[3]));
      ui->label_19->setText(QString::number(transferState[4]));
      // if (Acq->PLCState!=transferState)
      //{
      if (Acq->PLCState[1]==0&&transferState[1]==1){
          statusBar()->showMessage(tr("Połączono z PLC."));
        }

      Acq->y=transfer;
      Acq->ambientTemperature = ambientTemperature;

      Acq->PLCState=transferState;
      //          if (transferState[3]==1)
      //            {
      //              QMessageBox::information(this,"Drzwi otwarte","Zamknij drzwi komory aby kontunuować");
      //            }
      //          else if(transferState[4]==1)
      //            {
      //              QMessageBox::critical(this,"Awaria!","Awaria urządzenia. Nie można kontynuować pracy");
      //            }
      //          else
      //            {
      //              updateStates();
      //            }
      // }
    }
  //updateStates();
}

QVector<double> MainWindow::calculateCorrectedTemperatureVector(QVector<double> Ts, double To)
{
  const int W_12 = 12;
  double dT[W_12];
  QVector<double> correctedTemperatures(12);

  QVector<QVector<double>> a_ABC = Config->temperatureCalA;
  QVector<QVector<double>> b_ABC = Config->temperatureCalB;

  //obliczenie wektora poprawek
  for(int i=0; i<W_12; i++)
    {
      dT[i]=(((a_ABC[i][0]*To*To+a_ABC[i][1]*To+a_ABC[i][2])*Ts[i])+
          (b_ABC[i][0]*To*To+b_ABC[i][1]*To+b_ABC[i][2]));
      correctedTemperatures[i]=dT[i]+Ts[i];
    }
  return correctedTemperatures;
}

void MainWindow::setupDeviceData()
//inicjalizacja zmiennych początkowych
{


  QVector<int> data(5);
  data[0]=1;//gotowość do pracy PC
  data[1]=0;//praca start
  data[2]=0;//temperatura zadana 1
  data[3]=0;//temperatura zadana 2
  data[4]=0;//oświetlenie start
  for (int i=0;i<5;i++)
    {
      //            modbusDevice->setData(QModbusDataUnit::InputRegisters, 0,
      //                text.toInt(&ok, 16));

      modbusDevice->setData(QModbusDataUnit::HoldingRegisters, i,
                            data[i]); //10 - zapis dziesietny, 16 - zapis HEX
    }

}

void MainWindow::setRegister(const int &value, const int &address)
//ustaw rejestr Holding Register na wartość value typu INT o adresie address typu int
{
  if (!modbusDevice)
    return;

  bool ok = true;

  ok = modbusDevice->setData(QModbusDataUnit::HoldingRegisters, address, value);

  if (!ok)
    statusBar()->showMessage(tr("Nie można zapisać rejestru: ") + modbusDevice->errorString(),
                             5000);
}


void MainWindow::on_pushZrobZdjecie_clicked()
//uruchomienie nowego watku wykonujacego zdjecie
{
  Config->plcRegisters[4]=1; //wlacz swiatlo
  IDSCamera cam(Config->ID_Cam,Config->map1,Config->map2,
                Config->lightCalB,Config->lightCalG,Config->lightCalR, Config->lightCalOption,
                NULL,IS_PARAMETERSET_CMD_LOAD_EEPROM);
  cv::Mat img = cam.captureAndRectifyImage();

  //  QString tmpStr2=Config->dataPaths[0]+"/2.jpg";
  //  cv::Mat img =cv::imread(tmpStr2.toStdString());
  //  cv::resize(img,img,cv::Size(2048,2048));

  poly(Acq,2); //obliczenie wielomianu
  Config->plcRegisters[4]=0;//wylacz swiatlo

  if (img.rows>0&&img.cols>0)
    {
      cv::Mat tmp(img,cv::Rect(900,200,200,1600));
      Image1->fotoBGR=tmp;
      cvtColor(Image1->fotoBGR, Image1->fotoHSV,CV_BGR2HSV);
      //tmp.convertTo(Image1->fotoBGR, CV_8UC3);

      //    Image1->fotoBGR=img;
      //        Mat out=Image->fotoBGR;
      //          if (img.cols>0&&img.rows>0)
      //            {
      //              cv::resize(out, out, Size(0.2*out.cols, 0.2*out.rows), 0, 0, INTER_LINEAR);
      //              cv::imshow("after",out);
      //              out=img;
      //              cv::imshow("before",img);

      //            }
      //      koniec

      Data->name="Wykonane zdjęcie";
      Data->path="";
      //konwersja na HSV
      cvtColor(Image1->fotoBGR, Image1->fotoHSV,CV_BGR2HSV);
      //cvtColor(Image1->fotoHSV, Image1->fotoBGR,CV_HSV2BGR);
      QVector<double> temp(3),tmpH(Image1->fotoHSV.rows), tmpS(Image1->fotoHSV.rows),
          tmpV(Image1->fotoHSV.rows), tmpX(Image1->fotoHSV.rows);

      for (int j=0; j<Image1->fotoHSV.rows; j++)
        {
          temp=Image1->MeanRowHSV(Image1,j);
          //      tmpH[j]=temp[0];
          //      tmpS[j]=temp[1];
          //      tmpV[j]=temp[2];
          Vec3b intensity = Image1->fotoHSV.at<Vec3b>(j, 100);
          temp[0] = (double)intensity.val[0]; //HUE
          temp[1] = (double)intensity.val[1]; //Saturation
          temp[2] = (double)intensity.val[2]; //Value
          tmpH[j]=temp[0];
          tmpS[j]=temp[1];
          tmpV[j]=temp[2];

        }

      Data->yH=tmpH;
      Data->yS=tmpS;
      Data->yV=tmpV;
      computeYTemp(Acq,Data);
      captureImg(false);
      print(ui->wykres,Data,1);
    }

  else{
      QMessageBox::warning(this,"Nieprawidłowa konfiguracja","Nie polaczono z kamera 26.09.2016");
    }

}

void MainWindow::onImageCaptured(cv::Mat img)
//slot przechwytujacy wynik dzialania watku - wykonane zdjecie
{
  //  cv::Mat imgTmp=img;
  //cv::imshow("new",img);
  if (img.rows>0&&img.cols>0)
    {
      cv::Mat tmp(img,cv::Rect(924,224,200,1600)); //Rect(x,y,width,height)
      Image1->fotoHSV = tmp;
      Data->name="Wykonane zdjęcie";
      Data->path="";
      cvtColor(Image1->fotoHSV, Image1->fotoBGR,CV_HSV2BGR);
      QVector<double> temp(3),tmpH(Image1->fotoHSV.rows), tmpS(Image1->fotoHSV.rows),
          tmpV(Image1->fotoHSV.rows), tmpX(Image1->fotoHSV.rows);

      for (int j=0; j<Image1->fotoHSV.rows; j++)
        {
          temp=Image1->MeanRowHSV(Image1,j);

          tmpH[j]=temp[0];
          tmpS[j]=temp[1];
          tmpV[j]=temp[2];
        }
      Data->yH=tmpH;
      Data->yS=tmpS;
      Data->yV=tmpV;
      computeYTemp(Acq,Data);

      tmpH.clear();
      tmpS.clear();
      tmpV.clear();
      tmpX.clear();
      int i=0;
      for (int j=0; j<Data->yH.size();j++)
          //odrzucenie wszystkich probek, ktorcy V < 5
        {
          if(Data->yV[j]>5)
            {
              tmpH.append(Data->yH[j]);
              tmpS.append(Data->yS[j]);
              tmpV.append(Data->yV[j]);
              tmpX.append(Data->xTemp[j]);
              i++;
            }
        }

      Data->yH=tmpH;
      Data->yS=tmpS;
      Data->yV=tmpV;
      Data->xTemp=tmpX;
    }
  emit changeState();
}

void MainWindow::onUpdateProgressBar(int arg1)
{
  ui->progressBar->setValue(arg1);
}

void MainWindow::onMapsLoaded(cv::Mat _map1,cv::Mat _map2,
                              QVector<QVector<double>>_lightCalB,
                              QVector<QVector<double>>_lightCalG,
                              QVector<QVector<double>>_lightCalR,
                              QVector<QVector<double>>_temperatureCalA,
                              QVector<QVector<double>>_temperatureCalB)
{
  if (_lightCalB.size()>0&&_lightCalG.size()>0&&_lightCalR.size()>0){
      Config->lightCalB=_lightCalB;
      Config->lightCalG=_lightCalG;
      Config->lightCalR=_lightCalR;
    }
  else{
      Config->lightCalOption = 0;
      QMessageBox::warning(this,"Nieprawidłowa konfiguracja","Brak pliku kalibracji oświetlenia");
    }
  if (_temperatureCalA.size()>0&&_temperatureCalB.size()>0){
      Config->temperatureCalA=_temperatureCalA;
      Config->temperatureCalB=_temperatureCalB;
    }
  else{
      Config->temperatureCalOption = 0;
      QMessageBox::warning(this,"Nieprawidłowa konfiguracja","Brak pliku kalibracji temperatury");
    }
  if (_map1.cols>0&&_map2.cols>0){
      ui->menuBar->setVisible(true);
      Config->map1=_map1;
      Config->map2=_map2;
      initModbus();

    }
  else{
      QMessageBox::warning(this,"Nieprawidłowa konfiguracja","Brak pliku map transformacji");
    }
  ui->progressBar->setVisible(false);
  ui->label_Progress->setVisible(false);
  ui->actionKonfiguracja->setVisible(false);
  ui->actionKalibracja->setVisible(false);
  ui->menuBar->setVisible(true);
  setVisible("Test");

}

void MainWindow::on_pushButton_clicked()
{
  bool tempBool=stateInit->active();
  tempBool=stateLightsOn->active();
  emit this->changeState();
}

void MainWindow::on_actionLaboratorium_triggered()
{
  if (ui->actionLaboratorium->text()=="Laboratorium"){
      if (QMessageBox::question(this,"Potwierdź","Czy jesteś pewien, że chcesz włączyć tryb Laboratorium? "
                                "Wszelkie niezapisane dane zostaną utracone.",
                                QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
        {
          Password newPass(this);
          newPass.setPassword(Config->password);
          newPass.setModal(true);
          newPass.exec();
          if (newPass.getStatus()){

              ui->actionKonfiguracja->setVisible(true);
              ui->actionKalibracja->setVisible(true);

              Data->isData=false;
              Reference->isData=false;
              initializeGraph(ui->wykres,wykresTemp);
              ui->listWidget->clear();
              setVisible("Kalibracja");
              states->currentState=0;
              Config->plcRegisters[1]=0;
              Config->plcRegisters[2]=0;
              Config->plcRegisters[3]=0;
              Config->plcRegisters[4]=0;
              //                setRegister(0,1);
              //                setRegister(0,2);
              //                setRegister(0,3);
              ui->calibrateButton->setEnabled(true);
              ui->actionLaboratorium->setText("Tryb produkcyjny");
              ui->Opcja_Label->setText("Laboratorium");
              ui->Arrow_Label->setGeometry(110,10,21,21);
              ui->Tryb_Label->setGeometry(130,10,181,21);
            }
        }
    }
  else{
      if (QMessageBox::question(this,"Potwierdź","Czy jesteś pewien, że chcesz powrócić do Trybu produkcyjnego? "
                                "Wszelkie niezapisane dane zostaną utracone.",
                                QMessageBox::StandardButton::Ok,QMessageBox::StandardButton::Cancel)==1024)
        {
          ui->actionKonfiguracja->setVisible(false);
          ui->actionKalibracja->setVisible(false);

          Data->isData=false;
          Reference->isData=false;
          initializeGraph(ui->wykres,wykresTemp);
          ui->listWidget->clear();
          setVisible("Test");
          states->currentState=0;
          Config->plcRegisters[1]=0;
          Config->plcRegisters[2]=0;
          Config->plcRegisters[3]=0;
          Config->plcRegisters[4]=0;
          ui->calibrateButton->setEnabled(false);
          ui->actionLaboratorium->setText("Laboratorium");
          ui->Opcja_Label->setText("Tryb produkcyjny");
          ui->Arrow_Label->setGeometry(140,10,21,21);
          ui->Tryb_Label->setGeometry(160,10,181,21);
        }
    }
}

void MainWindow::on_reportButton_clicked()
{
  QString fileNameD=Data->path+"/"+Data->name+"_report.pdf";

  createPDF(fileNameD);
  QMessageBox::information(this,"Zakończono","Raport został pomyślnie wygenerowany. "
                                             "Plik raportu został zapisany w folderze: "+Data->path);
}

void MainWindow::createPDF(QString fileName)
{
  int marginX=200;
  int marginY=700;

  //test:
  //  OpenCSV(this,Data,"D:/IGOR/Coding/QT_Projects/build-HMI-Desktop_Qt_5_6_0_MSVC2015_64bit-Debug/FILES/file.csv");
  //  print(ui->wykres,Data,1);

  QPdfWriter writer(fileName);          ui->actionLaboratorium->setText("Laboratorium");

  QPainter painter(&writer);

  painter.setPen(Qt::black);
  QFont font;
  font.setPointSize(16);
  font.setBold(true);

  painter.setFont(font);
  painter.drawText(4000,marginY,"Raport z testu");

  font.setPointSize(12);
  font.setBold(false);
  painter.setFont(font);

  painter.drawText(marginX,marginY+=500,"Data: ");
  painter.drawText(3000,marginY,QDate::currentDate().toString(Qt::ISODate));

  painter.drawText(marginX,marginY+=250,"Godzina: ");
  painter.drawText(3000,marginY,QTime::currentTime().toString("hh:mm:ss"));

  painter.drawText(marginX,marginY+=250,"Numer zlecenia: ");
  painter.drawText(3000,marginY,Config->orderNo);

  painter.drawText(marginX,marginY+=250,"Numer próbki: ");
  painter.drawText(3000,marginY,Config->sampleNo);

  painter.drawText(marginX,marginY+=500,"Plik referencyjny: ");
  painter.drawText(3000,marginY,Reference->name);

  painter.drawText(marginX,marginY+=250,"Plik pomiarowy: ");
  painter.drawText(3000,marginY,Data->name);

  painter.drawText(marginX,marginY+=700,"Temperatury:");
  painter.drawText(3000,marginY,"Wzorzec");
  painter.drawText(6000,marginY,"Pomiar");
  painter.drawText(marginX,marginY+=250,"Tr: ");
  painter.drawText(3000,marginY, QString::number(qRound(Config->refTr*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(6000,marginY, QString::number(qRound(Config->pomTr*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(marginX,marginY+=250,"Tg: ");
  painter.drawText(3000,marginY, QString::number(qRound(Config->refTg*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(6000,marginY, QString::number(qRound(Config->pomTg*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(marginX,marginY+=250,"Th: ");
  painter.drawText(3000,marginY, QString::number(qRound(Config->refTh*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(6000,marginY, QString::number(qRound(Config->pomTh*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(marginX,marginY+=250,"Tb: ");
  painter.drawText(3000,marginY, QString::number(qRound(Config->refTb*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.drawText(6000,marginY, QString::number(qRound(Config->pomTb*1000.0)/1000.0,'f',3)+" [ᴼC]");

  painter.drawText(marginX,marginY+=500,"Błędy temperaturowe: ");

  painter.drawText(marginX,marginY+=250,"dTr: ");
  if (Config->dTrError==0)
    painter.setPen(Qt::darkGreen);
  else if (Config->dTrError==1)
    painter.setPen(QColor(255,127,39));//pomarańczowy
  else if (Config->dTrError==2)
    painter.setPen(Qt::red);
  painter.drawText(3000,marginY, QString::number(qRound(Config->dTr*1000.0)/1000.0,'f',3)+" [ᴼC]");

  painter.setPen(Qt::black);
  painter.drawText(marginX,marginY+=250,"dTg: ");
  if (Config->dTgError==0)
    painter.setPen(Qt::darkGreen);
  else if (Config->dTgError==1)
    painter.setPen(QColor(255,127,39));//pomarańczowy
  else if (Config->dTgError==2)
    painter.setPen(Qt::red);
  painter.drawText(3000,marginY, QString::number(qRound(Config->dTg*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.setPen(Qt::black);

  painter.drawText(marginX,marginY+=250,"dTh: ");
  if (Config->dThError==0)
    painter.setPen(Qt::darkGreen);
  else if (Config->dThError==1)
    painter.setPen(QColor(255,127,39));//pomarańczowy
  else if (Config->dThError==2)
    painter.setPen(Qt::red);
  painter.drawText(3000,marginY,QString::number(qRound(Config->dTh*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.setPen(Qt::black);

  painter.drawText(marginX,marginY+=250,"dTb: ");
  if (Config->dTbError==0)
    painter.setPen(Qt::darkGreen);
  else if (Config->dTbError==1)
    painter.setPen(QColor(255,127,39));//pomarańczowy
  else if (Config->dTbError==2)
    painter.setPen(Qt::red);
  painter.drawText(3000,marginY,QString::number(qRound(Config->dTb*1000.0)/1000.0,'f',3)+" [ᴼC]");
  painter.setPen(Qt::black);

  font.setBold(true);
  font.setPointSize(14);
  painter.setFont(font);
  painter.drawText(4000,marginY+=700,"Wynik testu:");


  if (Config->dTrError==0&&Config->dTgError==0&&Config->dThError==0&&Config->dTbError==0){
      painter.setPen(Qt::darkGreen);
      painter.drawText(4000,marginY+=500,"POZYTYWNY");
    }
  else if(Config->dTrError==2||Config->dTgError==2||Config->dThError==2||Config->dTbError==2){
      painter.setPen(Qt::red);
      painter.drawText(4000,marginY+=500,"NEGATYWNY");
    }
  else{
      painter.setPen(QColor(255,127,39)); //pomaranczowy
      painter.drawText(4000,marginY+=500,"POZYTYWNY (OSTRZEŻENIE)");
    }

  painter.setPen(Qt::black);
  font.setBold(false);
  font.setPointSize(12);
  painter.setFont(font);

  painter.drawText(marginX,marginY+=500,"Wykres danych pomiarowych i referencyjnych:");

  ui->wykres->item(0)->setVisible(false);//wyłączenie kursorów
  ui->wykres->item(1)->setVisible(false);
  ui->wykres->deselectAll();
  QPixmap newPixMap=ui->wykres->toPixmap(900,430);

  painter.drawPixmap(QRect(marginX,marginY+=500,9000,4300),
                     QPixmap(newPixMap));

  painter.end();
}

void MainWindow::on_checkBoxLightCal_toggled(bool checked)
{
  if (checked){
      Config->lightCalOption=1;
    } else{
      Config->lightCalOption=0;
    }

}
