#include "konfiguracja.h"
#include "ui_konfiguracja.h"
#include "mainwindow.h"
#include <opencv2/opencv.hpp>
#include <sstream>
#include <fstream>
#include <QVector>
#include <QDir>

int CHANNEL_NO=12;

Konfiguracja::Konfiguracja(QWidget *parent, QVector<QString> _paths,
                           QVector<double> _pixels,
                           int _ID_Cam) :
  QDialog(parent),
  uikonf(new Ui::Konfiguracja)
//konstruktor klasy Konfiguracja. Do konstruktora można przekazać ścieżki _paths oraz pixele _pixels.
//dzięki temu możliwe jest pokazanie już skonfigurowanych parametrów i sprawdzenie czy ścieżki do
//plików csv map transformacji są aktualne (pliki są bardzo duże i długo się ładują)
{
  uikonf->setupUi(this);
  pathX=_paths[0];
  pathY=_paths[1];
  pixels=_pixels;
  ID_Cam=_ID_Cam;
  status=false;
  uikonf->spinBoxT_1->setEnabled(true);
  uikonf->spinBoxT_2->setEnabled(true);
  uikonf->spinBoxT_3->setEnabled(true);
  uikonf->spinBoxT_4->setEnabled(true);
  uikonf->spinBoxT_5->setEnabled(true);
  uikonf->spinBoxT_6->setEnabled(true);
  uikonf->spinBoxT_7->setEnabled(true);
  uikonf->spinBoxT_8->setEnabled(true);
  uikonf->spinBoxT_9->setEnabled(true);
  uikonf->spinBoxT_10->setEnabled(true);
  uikonf->spinBoxT_11->setEnabled(true);
  uikonf->spinBoxT_12->setEnabled(true);

  uikonf->spinBoxT_1->setValue(_pixels[0]);
  uikonf->spinBoxT_2->setValue(_pixels[1]);
  uikonf->spinBoxT_3->setValue(_pixels[2]);
  uikonf->spinBoxT_4->setValue(_pixels[3]);
  uikonf->spinBoxT_5->setValue(_pixels[4]);
  uikonf->spinBoxT_6->setValue(_pixels[5]);
  uikonf->spinBoxT_7->setValue(_pixels[6]);
  uikonf->spinBoxT_8->setValue(_pixels[7]);
  uikonf->spinBoxT_9->setValue(_pixels[8]);
  uikonf->spinBoxT_10->setValue(_pixels[9]);
  uikonf->spinBoxT_11->setValue(_pixels[10]);
  uikonf->spinBoxT_12->setValue(_pixels[11]);

  uikonf->progressBar->setVisible(false);
  uikonf->labelLoading->setText("");
  uikonf->labelLoading->setVisible(true);
  uikonf->spinBoxIDCam->setValue(ID_Cam);
  //uikonf->pushOK->setEnabled(false);
  uikonf->lineEditGrad->setText(QDir::currentPath()+"/FILES/gradienty.csv");
  uikonf->lineEditVXQ->setText(QDir::currentPath()+"/FILES/vxq.csv");
  uikonf->lineEditVYQ->setText(QDir::currentPath()+"/FILES/vyq.csv");
  openGradient();
}

Konfiguracja::~Konfiguracja()
{

  delete uikonf;
}

int Konfiguracja::getID()
{
  return ID_Cam;
}

QVector<double> Konfiguracja::getValues()
{
  QVector<double> tmp(CHANNEL_NO);
  if (CHANNEL_NO>0)
    tmp[0]=double(uikonf->spinBoxT_1->value());
  if (CHANNEL_NO>1)
    tmp[1]=double(uikonf->spinBoxT_2->value());
  if (CHANNEL_NO>2)
    tmp[2]=double(uikonf->spinBoxT_3->value());
  if (CHANNEL_NO>3)
    tmp[3]=double(uikonf->spinBoxT_4->value());
  if (CHANNEL_NO>4)
    tmp[4]=double(uikonf->spinBoxT_5->value());
  if (CHANNEL_NO>5)
    tmp[5]=double(uikonf->spinBoxT_6->value());
  if (CHANNEL_NO>6)
    tmp[6]=double(uikonf->spinBoxT_7->value());
  if (CHANNEL_NO>7)
    tmp[7]=double(uikonf->spinBoxT_8->value());
  if (CHANNEL_NO>8)
    tmp[8]=double(uikonf->spinBoxT_9->value());
  if (CHANNEL_NO>9)
    tmp[9]=double(uikonf->spinBoxT_10->value());
  if (CHANNEL_NO>10)
    tmp[10]=double(uikonf->spinBoxT_11->value());
  if (CHANNEL_NO>11)
    tmp[11]=double(uikonf->spinBoxT_12->value());
  pixels=tmp;
  return pixels;
}

QVector<QString> Konfiguracja::getPaths()
{
  QVector<QString> tmp(2);
  tmp[0]=uikonf->lineEditVXQ->text();
  tmp[1]=uikonf->lineEditVYQ->text();
  return tmp;
}

void Konfiguracja::givePaths(QVector<QString> _paths)
//można wykorzystać tą metodę zamiast przekazywać ścieżki przez konstruktor
{
  QVector<QString> tmp=_paths;
  pathX=tmp[0];
  pathY=tmp[1];
}

void Konfiguracja::setPaths(const QString _pathX, const QString _pathY)
{
  pathX=_pathX;
  pathY=_pathY;
}

bool Konfiguracja::getStatus()
{
  return status;
}

QVector<int> Konfiguracja::getMax()
{
  return maxVec;
}

QVector<int> Konfiguracja::getMin()
{
  return minVec;
}

void Konfiguracja::on_spinBoxCh_valueChanged(int arg1)
{
  CHANNEL_NO=arg1;
  switch (arg1)
    {
    case 1:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(false);
        uikonf->spinBoxT_3->setEnabled(false);
        uikonf->spinBoxT_4->setEnabled(false);
        uikonf->spinBoxT_5->setEnabled(false);
        uikonf->spinBoxT_6->setEnabled(false);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }

    case 2:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(false);
        uikonf->spinBoxT_4->setEnabled(false);
        uikonf->spinBoxT_5->setEnabled(false);
        uikonf->spinBoxT_6->setEnabled(false);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 3:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(false);
        uikonf->spinBoxT_5->setEnabled(false);
        uikonf->spinBoxT_6->setEnabled(false);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 4:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(false);
        uikonf->spinBoxT_6->setEnabled(false);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 5:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(false);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 6:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(false);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 7:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(false);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 8:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(true);
        uikonf->spinBoxT_9->setEnabled(false);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 9:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(true);
        uikonf->spinBoxT_9->setEnabled(true);
        uikonf->spinBoxT_10->setEnabled(false);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 10:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(true);
        uikonf->spinBoxT_9->setEnabled(true);
        uikonf->spinBoxT_10->setEnabled(true);
        uikonf->spinBoxT_11->setEnabled(false);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 11:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(true);
        uikonf->spinBoxT_9->setEnabled(true);
        uikonf->spinBoxT_10->setEnabled(true);
        uikonf->spinBoxT_11->setEnabled(true);
        uikonf->spinBoxT_12->setEnabled(false);
        break;
      }
    case 12:
      {
        uikonf->spinBoxT_1->setEnabled(true);
        uikonf->spinBoxT_2->setEnabled(true);
        uikonf->spinBoxT_3->setEnabled(true);
        uikonf->spinBoxT_4->setEnabled(true);
        uikonf->spinBoxT_5->setEnabled(true);
        uikonf->spinBoxT_6->setEnabled(true);
        uikonf->spinBoxT_7->setEnabled(true);
        uikonf->spinBoxT_8->setEnabled(true);
        uikonf->spinBoxT_9->setEnabled(true);
        uikonf->spinBoxT_10->setEnabled(true);
        uikonf->spinBoxT_11->setEnabled(true);
        uikonf->spinBoxT_12->setEnabled(true);
        break;
      }
    }

}

void Konfiguracja::on_toolButtonVXQ_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, QFileDialog::tr("Otwórz mapę X. Plik CSV"), QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
  uikonf->lineEditVXQ->setText(fileName);
}

void Konfiguracja::on_toolButtonVYQ_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, QFileDialog::tr("Otwórz mapę Y. Plik CSV"), QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
  uikonf->lineEditVYQ->setText(fileName);
}

void Konfiguracja::setMap(const cv::Mat _map1, const cv::Mat _map2)
{
  map1=_map1;
  map2=_map2;
}


QVector<cv::Mat> Konfiguracja::getMaps()
{
  QVector<cv::Mat> tmp(2);
  tmp[0]=map1;
  tmp[1]=map2;
  return tmp;
}

void Konfiguracja::on_pushOK_clicked()
{
  setLabelText();
  openGradient();

  //openMaps();
  status=true;

}

void Konfiguracja::on_pushAnuluj_clicked()
{


}

void Konfiguracja::on_pushZapisz_clicked()
{
  openGradient();
  setLabelText();
  uikonf->toolButtonVXQ->setEnabled(false);
  uikonf->toolButtonVYQ->setEnabled(false);
  uikonf->pushOK->setEnabled(false);
  uikonf->pushZapisz->setEnabled(false);
  uikonf->lineEditVXQ->setEnabled(false);
  uikonf->lineEditVYQ->setEnabled(false);
  openMaps();
  uikonf->toolButtonVXQ->setEnabled(true);
  uikonf->toolButtonVYQ->setEnabled(true);
  uikonf->pushOK->setEnabled(true);
  uikonf->pushZapisz->setEnabled(true);
  uikonf->lineEditVXQ->setEnabled(true);
  uikonf->lineEditVYQ->setEnabled(true);
}

void Konfiguracja::openGradient()
{
  QString temp;
  QStringList temp2;
  double value0, value1;
  QVector<int> value0v,value1v;
  QString fileName = uikonf->lineEditGrad->text();
  if (!fileName.isNull())
    {
      QFile data(fileName);
      if (data.open(QFile::ReadOnly))
        {
          QTextStream input(&data);
          int i=0;
          while (!input.atEnd())
            {
              temp=input.readLine();
              temp2=temp.split(';');
              if (i>0)
                {
                  value0 = QString(temp2[0]).toDouble();
                  value1 = QString(temp2[1]).toDouble();

                  value0v += int(value0*1000);
                  value1v += int(value1*1000);

                }

              i++;
            }
          minVec=value0v;
          maxVec=value1v;

        }
      else
        {
          QMessageBox::warning(this, "Błąd", "Nie można otworzyć pliku.");

        }

    }
  else
    {
      QMessageBox::warning(this, "Nie wybrano pliku", "Należy wybrać prawidłowy plik do otwarcia.");

    }
}

void Konfiguracja::openMaps()
{
  if (uikonf->lineEditVXQ->text()!=pathX||uikonf->lineEditVYQ->text()!=pathY)
    //sprawdzenie czy pliki csv już nie są załadowane i nie ma konieczności ładować ich ponownie
    {
      int pVala=0, pValb=0;
      uikonf->progressBar->setVisible(true);
      uikonf->progressBar->setValue(pVala);
      cv::Mat tmpMap1(2048, 2048, CV_32FC1), tmpMap2(2048, 2048, CV_32FC1);
      cv::resize(tmpMap1, tmpMap1,cv::Size(2048,2048));
      setPaths(uikonf->lineEditVXQ->text(),uikonf->lineEditVYQ->text());
      std::ifstream vxq(pathX.toStdString());
      std::ifstream vyq(pathY.toStdString());
      std::string line;
      std::string value;
      std::stringstream ss;
      int x = 0;
      int y = 0;
      if(!std::getline(vxq, line)||!std::getline(vyq, line))
        {
          uikonf->labelLoading->setText("Błędna ścieżka do plików");

        }
      else
        {
          while (std::getline(vxq, line))
            {
              ss = std::stringstream(line);
              x = 0;
              while (std::getline(ss, value, ','))
                {
                  if (value != "NaN")
                    tmpMap1.at<float>(y, x) = std::stof(value);
                  ++x;
                }
              ++y;
              pVala=y;
              uikonf->progressBar->setValue(pVala);
              QCoreApplication::processEvents();
              //przechwytuje eventy okna podczas ładowania plików. Bardzo ważne
              //aby nie zawiesić aplikacji

            }
          y = 0;

          while (std::getline(vyq, line))
            {
              ss = std::stringstream(line);
              x = 0;
              while (std::getline(ss, value, ','))
                {
                  if (value != "NaN")
                    tmpMap2.at<float>(y, x) = std::stof(value);
                  ++x;
                }
              ++y;
              pValb=y;
              uikonf->progressBar->setValue(pVala+pValb);
              QCoreApplication::processEvents();
            }
          uikonf->labelLoading->setText("Pliki załadowane.");
          uikonf->progressBar->setValue(4096);
          status=true;
          uikonf->pushOK->setEnabled(false);
          setMap(tmpMap1, tmpMap2);
        }

    }
  else
    //jeśli pliki csv map transformacji są już załadowane
    {
      uikonf->labelLoading->setText("Załadowane pliki są aktualne.");
      status=true;
      uikonf->pushOK->setEnabled(false);
    }
}

void Konfiguracja::setLabelText()
//funkcja pomocnicza
{
  uikonf->labelLoading->setVisible(true);
  uikonf->labelLoading->setText("Ładowanie plików...");
}

void Konfiguracja::on_spinBoxIDCam_valueChanged(int arg1)
{
  ID_Cam=arg1;
}

void Konfiguracja::on_toolButtonGrad_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  QFileDialog::tr("Otwórz plik z gradientami. Plik CSV"), QDir::currentPath()+"/FILES/",
                                                  QFileDialog::tr("Plik CSV (*.csv)"));
  uikonf->lineEditGrad->setText(fileName);
  openGradient();
}
