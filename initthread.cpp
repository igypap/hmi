#include "initthread.h"
#include <QtCore>
#include <sstream>
#include <fstream>
#include <QMessageBox>

initThread::initThread(QObject *parent):QThread(parent)
{

}

void initThread::run()
{
  int pVala=0, pValb=0;
  cv::Mat tmpMap1(2048, 2048, CV_32FC1), tmpMap2(2048, 2048, CV_32FC1);
  QVector<QVector<double>> tmpLightCalB,tmpLightCalG,tmpLightCalR,tmpTemperatureCalA,tmpTemperatureCalB;
  QString pathX,pathY,pathB,pathG,pathR,pathTempA,pathTempB;
  pathX=paths[0];
  pathY=paths[1];
  pathB=paths[2];
  pathG=paths[3];
  pathR=paths[4];
  pathTempA=paths[5];
  pathTempB=paths[6];
  std::ifstream vxq(pathX.toStdString());
  std::ifstream vyq(pathY.toStdString());
  std::ifstream vB(pathB.toStdString());
  std::ifstream vG(pathG.toStdString());
  std::ifstream vR(pathR.toStdString());
  std::ifstream vTA(pathTempA.toStdString());
  std::ifstream vTB(pathTempB.toStdString());
  std::string line;
  std::string value;
  std::stringstream ss;
  int x = 0;
  int y = 0;

  if(!std::getline(vxq, line)||!std::getline(vyq, line))
    {
      cv::Mat tmpZero;
      tmpMap1=tmpZero;
      tmpMap2=tmpZero;

      //obsluga bledu nieprawidlowych plikow map transformacji

    }
  else
    {
      cv::resize(tmpMap1, tmpMap1,cv::Size(2048,2048));
      cv::resize(tmpMap2, tmpMap2,cv::Size(2048,2048));


      while(std::getline(vB,line)){
          ss = std::stringstream(line);
          x = 0;
          QVector<double>tempDVec(5);
          tmpLightCalB.append(tempDVec);
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpLightCalB[y][x] = std::stod(value);
              ++x;
            }
          ++y;

        }
      y = 0;
      while(std::getline(vG,line)){
          ss = std::stringstream(line);
          x = 0;
          QVector<double>tempDVec(5);
          tmpLightCalG.append(tempDVec);
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpLightCalG[y][x] = std::stod(value);
              ++x;
            }
          ++y;

        }
      y = 0;
      while(std::getline(vR,line)){
          ss = std::stringstream(line);
          x = 0;
          QVector<double>tempDVec(5);
          tmpLightCalR.append(tempDVec);
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpLightCalR[y][x] = std::stod(value);
              ++x;
            }
          ++y;

        }
      y = 0;
      while(std::getline(vTA,line)){
          ss = std::stringstream(line);
          x = 0;
          QVector<double>tempDVec(5);
          tmpTemperatureCalA.append(tempDVec);
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpTemperatureCalA[y][x] = std::stod(value);
              ++x;
            }
          ++y;

        }
      y = 0;

      while(std::getline(vTB,line)){
          ss = std::stringstream(line);
          x = 0;
          QVector<double>tempDVec(5);
          tmpTemperatureCalB.append(tempDVec);
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpTemperatureCalB[y][x] = std::stod(value);
              ++x;
            }
          ++y;

        }
      y = 0;

      while (std::getline(vxq, line))
        {
          ss = std::stringstream(line);
          x = 0;
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpMap1.at<float>(y, x) = std::stof(value);
              ++x;
            }
          ++y;
          pVala=y;
          emit updateProgressBar(pVala);
          QCoreApplication::processEvents();
          //przechwytuje eventy okna podczas ładowania plików. Bardzo ważne
          //aby nie zawiesić aplikacji
        }
      y = 0;

      while (std::getline(vyq, line))
        {
          ss = std::stringstream(line);
          x = 0;
          while (std::getline(ss, value, ','))
            {
              if (value != "NaN")
                tmpMap2.at<float>(y, x) = std::stof(value);
              ++x;
            }
          ++y;
          pValb=y;
          emit updateProgressBar(pVala+pValb);
          QCoreApplication::processEvents();
        }
      emit updateProgressBar(4096);

    }

  emit mapsLoaded(tmpMap1,tmpMap2, tmpLightCalB,tmpLightCalG,tmpLightCalR,
                  tmpTemperatureCalA,tmpTemperatureCalB);
}

void initThread::setPaths(QVector<QString> _paths)
{
  paths=_paths;
}
