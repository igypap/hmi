#include "password.h"
#include "ui_password.h"
#include <QMessageBox>

Password::Password(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Password)
{
  ui->setupUi(this);
  ui->pushButton_Ok->setEnabled(false);
  status=false;
  pass="";
}

Password::~Password()
{
  delete ui;
}

void Password::on_pushButton_Ok_clicked()
{
  if (ui->lineEdit->text()==pass){
      status=true;
      emit accept();
    }
  else{
      QMessageBox::warning(this,"Błędne hasło","Nieprawidłowe hasło. Wprowadź prawidłowe hasło.");
      ui->pushButton_Ok->setEnabled(false);
    }

}

void Password::on_lineEdit_textChanged(const QString &arg1)
{
  QString tmpStr=arg1;
  if (arg1!=""){
      ui->pushButton_Ok->setEnabled(true);
    }
  else{
      ui->pushButton_Ok->setEnabled(false);
    }
}

void Password::on_pushButton_Anuluj_clicked()
{
  emit reject();
}

bool Password::getStatus()
{
  return status;
}

void Password::setPassword(QString _pass)
{
  pass=_pass;
}
