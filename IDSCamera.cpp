//#include "stdafx.h"
#include "IDSCamera.h"
#include <opencv2\imgproc\imgproc.hpp>
#include "opencv2/opencv.hpp"
#include <QtCore>
#include <math.h>



IDSCamera::IDSCamera() :
  m_nCameraID(0),
  m_sIniPath(NULL),
  m_nParamsLoadingMode(IS_PARAMETERSET_CMD_LOAD_FILE)
{

}

IDSCamera::IDSCamera(
    const INT _camID,
    const cv::Mat _map1,
    const cv::Mat _map2,
    QVector<QVector<double>> _lightCalB,
    QVector<QVector<double>> _lightCalG,
    QVector<QVector<double>> _lightCalR,
    INT _option,
    wchar_t* _iniPath = NULL,
    UINT _paramsLoadingMode = IS_PARAMETERSET_CMD_LOAD_FILE
    )
  :
    m_nCameraID(_camID),
    m_map1(_map1),
    m_map2(_map2),
    m_sIniPath(_iniPath),
    m_nParamsLoadingMode(_paramsLoadingMode),
    m_lightCalB(_lightCalB),
    m_lightCalG(_lightCalG),
    m_lightCalR(_lightCalR),
    m_option(_option)
{
  if ((_paramsLoadingMode != IS_PARAMETERSET_CMD_LOAD_FILE) && (_paramsLoadingMode != IS_PARAMETERSET_CMD_LOAD_EEPROM))
    _paramsLoadingMode = IS_PARAMETERSET_CMD_LOAD_FILE;
}

IDSCamera::~IDSCamera()
{
  if (m_hCam != 0)
    {
      is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);
      is_ExitCamera(m_hCam);
    }
  //delete m_sIniPath;
}

INT IDSCamera::initCamera(HIDS *_hCam, HWND _hWnd)
{
  INT nRet = is_InitCamera(_hCam, _hWnd);


  ///////////////////////////////////////////////////////////////////
  //
  // Poni?ej sprawdzenie, czy wymagana jest aktualizacja firmware'u, aby urz?dzenie dzia?a?o z aktualnie
  // wykorzystywan? wersj? sterownika. Je?li aktualiacja jest wymagana, zostanie ona przeprowadzona automatycznie
  // po odkomentowaniu poni?szego "ifa". Mo?e to zaj?? do kilkunastu sekund, dlatego niezalecane jest odkomentowanie
  // w przypadku pracy przy linii produkcyjnej, bo mo?e to spowodowa? zak??cenie pracy i op??nienia. Firmware
  // lepiej aktualizowa? poprzez program IDS Camera Manager, dostarczany przez IDS z oprogramowaniem IDS Software Suite.
  //
  //////////////////////////////////////////////////////////////////

  //if (nRet == IS_STARTER_FW_UPLOAD_NEEDED)
  //{
  //	// Maksymalny czas oczekiwania na aktualizacj? firmware'u (domy?lnie 25 sekund)
  //	INT nUploadTime = 25000;
  //	is_GetDuration(*hCam, IS_STARTER_FW_UPLOAD, &nUploadTime);
  //	*hCam = (HIDS)(((INT)*hCam) | IS_ALLOW_STARTER_FW_UPLOAD);
  //	nRet = is_InitCamera(hCam, hWnd);
  //}

  return nRet;
}

bool IDSCamera::openCamera()
{
  if (m_hCam != 0)
    {
      // zwolnienie pami?ci zawieraj?cej poprzedni obraz:
      is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);
      is_ExitCamera(m_hCam);
    }

  // otwarcie kamery o odpowiednim ID:
  m_hCam = (HIDS) m_nCameraID;
  INT ret = initCamera(&m_hCam, NULL);

  if (ret == IS_SUCCESS)
    {
      // pobieranie informacji o matrycy:
      is_GetSensorInfo(m_hCam, &m_sInfo);

      INT paramRet = loadCameraParameters(m_nParamsLoadingMode);
      if (paramRet != IS_SUCCESS)
        {
          getMaxImageSize(&m_nSizeX, &m_nSizeY);

          // inicjalizacja pami?ci w celu zastosowania DIB (Device Independent Bitmap):
          is_AllocImageMem(m_hCam,
                           m_nSizeX,
                           m_nSizeY,
                           m_nBitsPerPixel,
                           &m_pcImageMemory,
                           &m_lMemoryId);
          // aktywacja pami?ci - wykonane zdj?cia zostan? zapisane w buforze m_pcImageMemory o ID m_lMemoryId:
          is_SetImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);

          // inicjalizacja wy?wietlania:
          IS_SIZE_2D imageSize;
          imageSize.s32Width = m_nSizeX;
          imageSize.s32Height = m_nSizeY;

          is_AOI(m_hCam, IS_AOI_IMAGE_SET_SIZE, (void*)&imageSize, sizeof(imageSize));

          is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);
        }

      // ustawienie g??bi bitowej odpowiadaj?cej aktualnym ustawieniom systemu Windows.
      // Je?li uda?o nam si? po?aczy? z kamer?, nie zapisujemy do zmiennej m_nBitsPerPixel,
      // jak w wywo?aniu tej?e funkcji powy?ej, gdy? warto?? zmiennej zosta?a ju? ustawiona przez
      // funkcj? loadCameraParameters().
      INT tempColorDepth=32;
      is_GetColorDepth(m_hCam, &tempColorDepth, &m_nColorMode);
      is_SetColorMode(m_hCam, m_nColorMode);

      is_SetDisplayMode(m_hCam, IS_SET_DM_DIB);

      // wy??cz raportowanie b??d?w (aby w??czy?, nale?y jako argument poda? flag? IS_ENABLE_ERR_REP):
      ret = is_SetErrorReport(m_hCam, IS_DISABLE_ERR_REP);
      if (ret != IS_SUCCESS)
        {
          // Nie mo?na w??czy? automatycznego zg?aszania b??d?w uEye. W tym miejscu mo?na do??czy? kod, kt?ry
          // okre?li, jak powinno to zosta? zg?oszone (log do pliku, komunikat w konsoli, okienko Qt...)
          return false;
        }

    }
  else
    {
      // Nie uda?o si? po??czy? z kamer? uEye. W tym miejscu mo?na do??czy? kod, kt?ry
      // okre?li, jak powinno to zosta? zg?oszone (log do pliku, komunikat w konsoli, okienko Qt...)
      return false;
    }

  return true;
}

void IDSCamera::getMaxImageSize(INT *_pnSizeX, INT *_pnSizeY)
{
  // sprawdzenie, czy kamera wspiera wybieralne AOI
  // jedynie uEye xs nie wspiera tej funkcji
  INT nAOISupported = 0;
  BOOL bAOISupported = TRUE;
  if (is_ImageFormat(m_hCam,
                     IMGFRMT_CMD_GET_ARBITRARY_AOI_SUPPORTED,
                     (void*)&nAOISupported,
                     sizeof(nAOISupported)) == IS_SUCCESS)
    {
      bAOISupported = (nAOISupported != 0);
    }

  if (bAOISupported)
    {
      // ustawienie maksymalnego mo?liwego rozmiaru zdj?cia:
      SENSORINFO sInfo;
      is_GetSensorInfo(m_hCam, &sInfo);
      *_pnSizeX = sInfo.nMaxWidth;
      *_pnSizeY = sInfo.nMaxHeight;
    }
  else
    {
      // Dotyczy jedynie uEye xs:
      IS_SIZE_2D imageSize;
      is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

      *_pnSizeX = imageSize.s32Width;
      *_pnSizeY = imageSize.s32Height;
    }
  return;
}

cv::Mat IDSCamera::captureImage()
{
  openCamera();
  //QCoreApplication::processEvents();

  cv::Mat img;
  // W warunku if - wykonanie zdj?cia i zapisanie do bufora m_pcImageMemory:
  if (is_FreezeVideo(m_hCam, IS_WAIT) == IS_SUCCESS)
    //QCoreApplication::processEvents();
    img = cv::Mat(m_nSizeX, m_nSizeY, CV_8UC4, m_pcImageMemory);

  return img;
}

cv::Mat IDSCamera::captureAndRectifyImage()
{
  cv::Mat img = captureImage(); //wykonaj zdjęcie

  if (img.empty())
    return img; //pusty obraz
  img.convertTo(img, CV_8UC3); //usunięcie kanału alpha - do przekształcenie na HSV

  //****************************************
  //kod kalibrujacy oswietlenie
  if (m_option==1){ //czy wykonywać korekcję oświetlenia? jeżeli 1 to tak (pobrane z pliku .ini)
      QVector<double> temp(3);
      for (int row=0; row<img.rows;row++)
        {
          for (int i=0; i<img.cols; i++)
            {
              cv::Vec3b intensity = img.at<cv::Vec3b>(row, i);
              temp[0] = (double)intensity.val[0]; //B wartosc 0..255
              temp[1] = (double)intensity.val[1]; //G wartosc 0..255
              temp[2] = (double)intensity.val[2]; //R wartosc 0..255

              temp[0]=temp[0]*m_lightCalB[row][0]; //problem, ponieważ m_lightCalB jest wektorem wektorów (dwa wymiary)
              temp[1]=temp[1]*m_lightCalG[row][0]; //problem, ponieważ m_lightCalG jest wektorem wektorów (dwa wymiary)
              temp[2]=temp[2]*m_lightCalR[row][0]; //problem, ponieważ m_lightCalR jest wektorem wektorów (dwa wymiary)


              temp[0]=round(temp[0]);
              temp[1]=round(temp[1]);
              temp[2]=round(temp[2]);

              intensity.val[0] = (uchar)temp[0];
              intensity.val[1] = (uchar)temp[1];
              intensity.val[2] = (uchar)temp[2];

              img.at<cv::Vec3b>(row,i)=intensity; //przypisanie wartości wektorów B, G, R do piksela typu Mat
            }
        }

    }

  //koniec kodu kalibrujacego oswietlenie
  //****************************************

  else if (m_option==0){

    }
  cv::cvtColor(img, img,CV_BGR2HSV); //konwersja do HSV
  //sprawdzenie warunku czy można przeprowadzić "prostowanie" obrazu
  cv::Mat out(img.rows, img.cols, img.type());
  if ((out.rows != m_map1.rows) || (out.cols != m_map1.cols))
    return cv::Mat();

  //prostowanie obrazu na podstawie map transformacji
  cv::remap(img, out, m_map1, m_map2, cv::INTER_LINEAR);

  return out;
}

void IDSCamera::setMap1(const cv::Mat _map1)
{
  m_map1 = _map1;
  return;
}

void IDSCamera::setMap2(const cv::Mat _map2)
{
  m_map2 = _map2;
  return;
}

void IDSCamera::setIniPath(wchar_t* _iniPath)
{
  m_sIniPath = _iniPath;
}

cv::Mat IDSCamera::getMap1()
{
  return m_map1;
}

cv::Mat IDSCamera::getMap2()
{
  return m_map2;
}

wchar_t* IDSCamera::getIniPath()
{
  return m_sIniPath;
}

INT IDSCamera::loadCameraParameters(UINT _command)
{
  if ((_command != IS_PARAMETERSET_CMD_LOAD_FILE) && (_command != IS_PARAMETERSET_CMD_LOAD_EEPROM))
    return IS_INCOMPATIBLE_SETTING;

  INT ret;
  if (_command == IS_PARAMETERSET_CMD_LOAD_FILE)
    ret = is_ParameterSet(m_hCam, _command, m_sIniPath, NULL);
  else
    ret = is_ParameterSet(m_hCam, _command, NULL, NULL);

  if (ret == IS_SUCCESS)
    {
      // dealokacja uprzednio zaalokowanej pami?ci:
      is_FreeImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);

      IS_SIZE_2D imageSize;
      is_AOI(m_hCam, IS_AOI_IMAGE_GET_SIZE, (void*)&imageSize, sizeof(imageSize));

      INT nAllocSizeX = 0;
      INT nAllocSizeY = 0;

      m_nSizeX = nAllocSizeX = imageSize.s32Width;
      m_nSizeY = nAllocSizeY = imageSize.s32Height;

      UINT nAbsPosX = 0;
      UINT nAbsPosY = 0;

      is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_X_ABS, (void*)&nAbsPosX, sizeof(nAbsPosX));
      is_AOI(m_hCam, IS_AOI_IMAGE_GET_POS_Y_ABS, (void*)&nAbsPosY, sizeof(nAbsPosY));

      if (nAbsPosX)
        {
          nAllocSizeX = m_sInfo.nMaxWidth;
        }
      if (nAbsPosY)
        {
          nAllocSizeY = m_sInfo.nMaxHeight;
        }

      switch (is_SetColorMode(m_hCam, IS_GET_COLOR_MODE))
        {
        case IS_CM_RGBA12_UNPACKED:
        case IS_CM_BGRA12_UNPACKED:
          m_nBitsPerPixel = 64;
          break;

        case IS_CM_RGB12_UNPACKED:
        case IS_CM_BGR12_UNPACKED:
        case IS_CM_RGB10_UNPACKED:
        case IS_CM_BGR10_UNPACKED:
          m_nBitsPerPixel = 48;
          break;

        case IS_CM_RGBA8_PACKED:
        case IS_CM_BGRA8_PACKED:
        case IS_CM_RGB10_PACKED:
        case IS_CM_BGR10_PACKED:
        case IS_CM_RGBY8_PACKED:
        case IS_CM_BGRY8_PACKED:
          m_nBitsPerPixel = 32;
          break;

        case IS_CM_RGB8_PACKED:
        case IS_CM_BGR8_PACKED:
          m_nBitsPerPixel = 24;
          break;

        case IS_CM_BGR565_PACKED:
        case IS_CM_UYVY_PACKED:
        case IS_CM_CBYCRY_PACKED:
          m_nBitsPerPixel = 16;
          break;

        case IS_CM_BGR5_PACKED:
          m_nBitsPerPixel = 15;
          break;

        case IS_CM_MONO16:
        case IS_CM_SENSOR_RAW16:
        case IS_CM_MONO12:
        case IS_CM_SENSOR_RAW12:
        case IS_CM_MONO10:
        case IS_CM_SENSOR_RAW10:
          m_nBitsPerPixel = 16;
          break;

        case IS_CM_RGB8_PLANAR:
          m_nBitsPerPixel = 24;
          break;

        case IS_CM_MONO8:
        case IS_CM_SENSOR_RAW8:
        default:
          m_nBitsPerPixel = 8;
          break;
        }

      // inicjalizacja pami?ci:
      is_AllocImageMem(m_hCam, nAllocSizeX, nAllocSizeY, m_nBitsPerPixel, &m_pcImageMemory, &m_lMemoryId);

      is_SetImageMem(m_hCam, m_pcImageMemory, m_lMemoryId);	// aktywacja pami?ci.

      // inicjalizacja wy?wietlania:
      imageSize.s32Width = m_nSizeX;
      imageSize.s32Height = m_nSizeY;

      is_AOI(m_hCam, IS_AOI_IMAGE_SET_SIZE, (void*)&imageSize, sizeof(imageSize));
    }

  return ret;
}
