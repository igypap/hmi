#include "summary.h"
#include "ui_summary.h"

Summary::Summary(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Summary)
{
  ui->setupUi(this);
  dTr=dTg=dTh=dTb=0.0;
}

Summary::~Summary()
{
  delete ui;
}

void Summary::setErrors(double _dTr, double _dTg, double _dTh, double _dTb, QString _result)
{
  dTr=_dTr;
  dTg=_dTg;
  dTh=_dTh;
  dTb=_dTb;
  result=_result;
  ui->label_Blad_Tr->setText(QString::number(qRound(dTr*1000.0)/1000.0,'f',3)+" [ᴼC]");
  ui->label_Blad_Tg->setText(QString::number(qRound(dTg*1000.0)/1000.0,'f',3)+" [ᴼC]");
  ui->label_Blad_Th->setText(QString::number(qRound(dTh*1000.0)/1000.0,'f',3)+" [ᴼC]");
  ui->label_Blad_Tb->setText(QString::number(qRound(dTb*1000.0)/1000.0,'f',3)+" [ᴼC]");
  if (result=="POZYTYWNY")
    ui->label_Wynik->setText("<font color=green>"+result+"</font>");
  else if(result=="OSTRZEZENIE"){
      ui->label_Wynik->setText("<font color=orange>"+result+"</font>");
    }
  else
    ui->label_Wynik->setText("<font color=red>"+result+"</font>");


}

void Summary::on_pushButton_Ok_clicked()
{
  emit accept();
}
