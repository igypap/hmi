#ifndef SUMMARY_H
#define SUMMARY_H

#include <QDialog>
#include <QString>

namespace Ui {
  class Summary;
}

class Summary : public QDialog
{
  Q_OBJECT

public:
  explicit Summary(QWidget *parent = 0);
  ~Summary();
  void setErrors(double _dTr,double _dTg,double _dTh,double _dTb, QString _result);


private slots:
  void on_pushButton_Ok_clicked();

private:
  Ui::Summary *ui;
  double dTr,dTg,dTh,dTb;
  QString result;
};

#endif // SUMMARY_H
