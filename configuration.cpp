#include "configuration.h"
#include "ui_configuration.h"
#include "mainwindow.h"

Configuration::Configuration(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Configuration)
{
  ui->setupUi(this);
  config=new ConfigClass(this);
  status=false;
  ui->doubleSpinBox_STr->setVisible(false);
  ui->label_6->setVisible(false);
}

Configuration::~Configuration()
{
  delete ui;
}

void Configuration::setConfigData(ConfigClass *_Config)
{
  config=_Config;
  ui->doubleSpinBox_HTr->setValue(config->H_Tr);
  ui->doubleSpinBox_STr->setValue(config->S_Tr);
  ui->doubleSpinBox_HTg->setValue(config->H_Tg);
  ui->doubleSpinBox_HTh->setValue(config->H_Th);
  ui->doubleSpinBox_HTb->setValue(config->H_Tb);
  ui->doubleSpinBox_HD_H->setValue(config->HD_Tr_H);
  ui->doubleSpinBox_HD_HH->setValue(config->HD_Tr_HH);
  ui->lineEdit_Folia_I->setText(config->refPaths[0]);
  ui->lineEdit_Folia_Ia->setText(config->refPaths[1]);
  ui->lineEdit_Folia_II->setText(config->refPaths[2]);
  ui->lineEdit_Dane->setText(config->dataPaths[0]);
  ui->lineEdit_Raporty->setText(config->dataPaths[1]);
}

bool Configuration::getStatus()
{
  return status;
}

void Configuration::on_pushButton_Ok_clicked()
{
  QVector<QString> refPathsTemp(2);
  config->H_Tr=ui->doubleSpinBox_HTr->value();
  config->S_Tr=ui->doubleSpinBox_STr->value();
  config->H_Tg=ui->doubleSpinBox_HTg->value();
  config->H_Th=ui->doubleSpinBox_HTh->value();
  config->H_Tb=ui->doubleSpinBox_HTb->value();

  config->HD_Tr_H=ui->doubleSpinBox_HD_H->value();
  config->HD_Tr_HH=ui->doubleSpinBox_HD_HH->value();
  config->HD_Tg_H=ui->doubleSpinBox_HD_H->value();
  config->HD_Tg_HH=ui->doubleSpinBox_HD_HH->value();
  config->HD_Th_H=ui->doubleSpinBox_HD_H->value();
  config->HD_Th_HH=ui->doubleSpinBox_HD_HH->value();
  config->HD_Tb_H=ui->doubleSpinBox_HD_H->value();
  config->HD_Tb_HH=ui->doubleSpinBox_HD_HH->value();

  refPathsTemp[0]=ui->lineEdit_Dane->text();
  refPathsTemp[1]=ui->lineEdit_Raporty->text();
  config->dataPaths=refPathsTemp;

  refPathsTemp.append("");

  refPathsTemp[0]=ui->lineEdit_Folia_I->text();
  refPathsTemp[1]=ui->lineEdit_Folia_Ia->text();
  refPathsTemp[2]=ui->lineEdit_Folia_II->text();
  config->refPaths=refPathsTemp;
  status=true;
  emit accept();
}

void Configuration::on_pushButton_Anuluj_clicked()
{
  emit reject();
}

void Configuration::on_toolButton_Folia_I_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, QFileDialog::tr("Wskaż ścieżkę do pliku Referencja I. "
                                                                        "Plik CSV"), QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
  ui->lineEdit_Folia_I->setText(fileName);
}

void Configuration::on_toolButton_Folia_Ia_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, QFileDialog::tr("Wskaż ścieżkę do pliku Referencja Ia. "
                                                                        "Plik CSV"), QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
  ui->lineEdit_Folia_Ia->setText(fileName);
}

void Configuration::on_toolButton_Folia_II_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, QFileDialog::tr("Wskaż ścieżkę do pliku Referencja II. "
                                                                        "Plik CSV"), QDir::currentPath()+"/FILES/", QFileDialog::tr("Plik CSV (*.csv)"));
  ui->lineEdit_Folia_II->setText(fileName);
}

void Configuration::on_toolButton_Dane_clicked()
{
  QString fileName = QFileDialog::getExistingDirectory(this, QFileDialog::tr("Wskaż domyślną ścieżkę "
                                                                             "zapisu danych pomiarowych."), QDir::currentPath()+"/FILES/",
                                                       QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
  ui->lineEdit_Dane->setText(fileName);
}

void Configuration::on_toolButton_Raporty_clicked()
{
  QString fileName = QFileDialog::getExistingDirectory(this, QFileDialog::tr("Wskaż domyślną ścieżkę "
                                                                             "zapisu raportów."), QDir::currentPath()+"/FILES/",
                                                       QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
  ui->lineEdit_Raporty->setText(fileName);
}
