#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "opencv2/opencv.hpp"
#include "uEye.h"
#include <QButtonGroup>
#include <QMainWindow>
#include <QModbusServer>
#include "threadclass.h"
#include "initthread.h"


namespace Ui {
class MainWindow;

}


class StatesClass : public QObject
{
  Q_OBJECT
public:
  explicit StatesClass(QObject *parent=0);
  ~StatesClass();
  int screen; //0-inicjalizacja, 1-Test, 2-Kalibracja, 3-Analiza
  int statePLC; //
  int currentState;
  bool imageCaptured;
};

class DataClass :public QObject
    //klasa obiektu przechowującego dane do wykresu (temperatura i wartości Hue, Saturation Value)
{
  Q_OBJECT

public:
  explicit DataClass(QObject *parent=0);
  ~DataClass();

  QString path, name, date, time;
  QVector<double> xTemp, yH, yS, yV; // wartości temperatury oraz HSV
  bool isData; //wartość TRUE jeśli dane w klasie DataClass są prawidłowe
  void clear(); //czyści dane w obiekcie typu DataClass

};

class AcqClass //klasa obiektu przechowującego wartości x,y oraz współczynniki wielomianu
    : public QObject
    {
      Q_OBJECT
public:
  explicit AcqClass(QObject *parent=0);
  ~AcqClass(); 
  double ambientTemperature;
  QVector<double> x,y,polyCoef;
 // QVector<double> PLCTemp;
  QVector<int> PLCState;
    // vector x - numer piksela, y - wartość temperatury, oraz wpsołczynniki wielomianu
  void setData(QVector<double> _yT, QVector<double> _x);
    //przypisanie wartości _yT do wartości y oraz _x do x z klasy AcqClass

};

class ImageClass //klasa obiektu przechowującego obraz w formacie OpenCV Mat
    : public QObject
    {
      Q_OBJECT
public:
   explicit ImageClass(QObject *parent=0);
   ~ImageClass();

  QString path, name;
  cv::Mat fotoBGR, fotoHSV; // obrazy w formacie BGR i HSV

  QVector<double> MeanRowHSV(ImageClass *Image, int row);
    //maetoda licząca średnią wartość pikseli dla poszczególnych kanałów HSV w daneym wierszu ROW
    //Należy podać wiersz ROW dla którego ma zostać policzona średnia
  int OpenMatFile(QWidget *parent, ImageClass *Image, DataClass *Data);
  //Otwarcie pliku Mat z dysku

};

class ConfigClass
    //klasa obiektu przechowującego dane konfiguracyjne
    : public QObject
    {
      Q_OBJECT
public:
  explicit ConfigClass(QObject *parent=0);
  ~ConfigClass();

  //funkcja pobiera mapy transformacji z dysku i zapisuje w zmiennych map1 i map2
  void setMaps(cv::Mat _map1, cv::Mat _map2); //setter zmiennych klasy

  void calcGradientVecs(QVector<double> _params);

  QVector<double> pixels; //vector pixeli dla odpowiednich kanałów pomiaru temperatury
  QVector<QString> paths; //ścieżki do plików map transformacji, kalibracji oswietlenia i temperatury
  QVector<QString> refPaths; //ścieżki do plików referencyjnych ustawiane w "Laboratorium"
  QVector<QString> dataPaths;
  QVector<double> min; //wektor wartości MIN gradientu
  QVector<double> max; //wektor wartości MAX gradientu
  QString password; //hasło do przełączenia trybu Produkcja->Laboratorium
  QString sampleNo;
  QString orderNo;
  QVector<int> plcRegisters;
  double cMin;
  double cMax;
  double cWindow;
  double cStep;
  double H_Tr;
  double S_Tr;
  double H_Tg;
  double H_Th;
  double H_Tb;
  double HD_Tr_H;
  double HD_Tr_HH;
  double HD_Tg_H;
  double HD_Tg_HH;
  double HD_Th_H;
  double HD_Th_HH;
  double HD_Tb_H;
  double HD_Tb_HH;
  double Ref_I_MIN;
  double Ref_I_MAX;
  double Ref_Ia_MIN;
  double Ref_Ia_MAX;
  double Ref_II_MIN;
  double Ref_II_MAX;

  double dTr;
  double dTg;
  double dTh;
  double dTb;

  double refTr;
  double refTg;
  double refTh;
  double refTb;

  double pomTr;
  double pomTg;
  double pomTh;
  double pomTb;

  int dTrError; //0 - zielony, 1- żółty, 2-czerwony
  int dTgError;
  int dThError;
  int dTbError;

  QString result;


  cv::Mat map1, map2; //mapy transformacji
  QVector<QVector<double>> lightCalB; //wektory przetrzymujace wspolczynniki kalibracji oswietlenia obrazu
  QVector<QVector<double>> lightCalG;
  QVector<QVector<double>> lightCalR;
  QVector<QVector<double>> temperatureCalA;
  QVector<QVector<double>> temperatureCalB;
  int lightCalOption; //1 - wykonaj kalibracje oswietlenia, 0 - nie wykonuj
  int temperatureCalOption; //1 - wykonaj kalibracje oswietlenia, 0 - nie wykonuj
  int ID_Cam; //ID kamery
  bool status; //TRUE - jeśli konfiguracja została przeprowadzona

};

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    DataClass *Data;
    ImageClass *Image1;
    DataClass *Reference;
    AcqClass *Acq;
    ConfigClass *Config;
    QCustomPlot *wykresTemp;
    QCPDataMap *dataMap;
    QCPDataMap *dataMap0;
    QCPDataMap *dataMap1;
    QCPDataMap *dataMap2;
    QCPDataMap *dataMap3;
    QCPDataMap *dataMap4;
    QCPDataMap *dataMap5;
    QCPDataMap *dataMap6;
    QCPDataMap *dataMap7;
    QCPDataMap *dataMap8;
    QCPDataMap *dataMap9;
    StatesClass *states;
    threadClass *mThread;
    initThread *mInitThread;
    QStateMachine *machine;
    QState *stateInit;
    QState *stateIdle;
    QState *stateStartCalibrate;
    QState *stateSendSetpoints;
    QState *stateSetpointsSent;
    QState *stateLightsOn;
    QState *stateCaptureImgCal;
    QState *stateImageCaptured;
    QState *stateIsCalibrateFinished;
    QTimer *timer;
    QTimer *timerReplot;
public slots:
    void onImageCaptured(cv::Mat);
    void onUpdateProgressBar(int);
    void onMapsLoaded(cv::Mat, cv::Mat,
                      QVector<QVector<double> > _lightCalB,
                      QVector<QVector<double> > _lightCalG,
                      QVector<QVector<double> > _lightCalR,
                      QVector<QVector<double> > _temperatureCalA,
                      QVector<QVector<double> > _temperatureCalB);

signals:
    void changeState();
    void changeStateToInit();
    void changeStateToSendSetpoints();

public:

    void print(QCustomPlot *customPlot, DataClass *Data, int plotNo);
      //rysuj wykres. Należy podać obiekt DataClass z danymi do wykresu.
      //plotNo oznacza czy rysujemy referencje czy zwykłe dane, 1 - zwykłe dane, 2 - referencja
    void autoScale(QCustomPlot *customPlot, DataClass *Data, DataClass *Reference);
      //autoskalowanie wykresu
    void initializeGraph(QCustomPlot *customPlot, QCustomPlot *_temp);
      //inicjalizacja wykresu: czyszczenie, ustawienie osi i przygotowanie wykresów
    int setGradient(QWidget *parent);
      //ustawienie gradientu temperatury podczas wykonywania "Testu"
    int OpenCSV(QWidget *parent, DataClass *_Data, QString _fileName="");
      //otwarcie pliku CSV z danymi pomiarowymi (temperatury vs HSV)
    int SaveToCSV(QWidget *parent, DataClass *Data, QString _fileName="");
      //zapisanie danych pomiarowych (temperatury vs HSV) do pliku CSV.
    void setVisible(QString tryb);
      //ustaw wlasciwosc setVisible dla elementow ui. Tryby:
      //"Nothing" - zaden element nie widoczny
      //"Analiza" - elementy dla opcji analiza
      //"Test" - elementy dla opcji test
      //"Kalibracja" - elementy dla opcji kalibracja
    void readPLC();
      //odczytaj danez PLC

    void poly(AcqClass *Acq, int degree);
      //funkcja wyznaczająca wielomian aproksymujący dane z obiektu klasy AcqClass
      //degree - stopień wielomiany. 2 dla wielomianu kwadratowego
    void computeYTemp(AcqClass *_Acq, DataClass *_Data);
      //przygotuj dane dla klasy DataClass na podstawie danych z akwizycji
      //(AcqClass z policzonymi współczynnikami wielomianu aproksymującego)
    void getImage();
    //pobierz obraz z kamery
    void captureImg(bool sortMean);
      //obsługa pobrania zdjęcia oraz uruchomienie metody getImage
    void sort(DataClass *_Data, int p, int r);
      //sortowanie elementów y oraz xTemp obiektu klasy DataClass
      //sortowanie generuje monotoniczną funkcję y(x)
    int partition(DataClass *_Data, int p, int r);
      //funkcja pomocnicza do funkcji sort
    void MeanData(DataClass *_Data, int step);
      //średnia krocząca wartości yH, yS,yV z kroku o długości "step"
      //step musi być liczbą podzielną przez 2
    void updateStates();
      //obsluga stanów pracy podczas komunikacji z PLC.
    bool captureCalibrate();
      //pobranie zdjęcia w trybie kalibracji (wykonywania wzorca)
    void openConfigFile();
      //zaladowanie danych z pliku konfiguracyjnego .ini
    void saveConfigFile();
      //zapisa danych do pliku konfiguracyjnego .ini
    void initConfig();
      //inicjalizacja konfiguracji. Odczyt ustawień z pliku konfiguracyjnego, załadowanie
      //map transformacji poprzez metode getMaps()
    void initStateMachine();
      //inicjalizacja maszyny stanów do obsługi połączenia z PLC i wykonywania zdjęć w trybie automatycznym
    void getMaps();
      //załadowanie map transformacji do pamięci
    void connectToPLC();
      //funkcja wywołująca połączenie MODBUS z PLC (jeśli nie postawione)
      //lub rozłączająca połączenie, gdy zostało wcześniej nawiązane. Można wywołać dodatkowo,
      //gdy automatyczne połączenie nie działa
    void calculateTempErrors();
      //obliczenie wartości parametrów barwowej odpowiedzi temperaturowej i uaktualnienie kontrolek UI
      //do wyswietlenia wartosci Tr. Tb, Th, Tg i ich bledow
    void createPDF(QString fileName);
      //funkcja PDF creator do generowania raportów
    QVector<double> calculateCorrectedTemperatureVector(QVector<double> Ts, double To);
      //funkcja wykonująca korekcję wektora temperatur Ts[] na podstawie temperatury otoczenia To





    double calculateParameter(QVector<double> yData, QVector<double> xData, double key);

private slots:
     void on_openMat_clicked();

     void on_OpenFileButton_clicked();

     void on_SaveFileButton_clicked();

     void on_actionAbout_triggered();

     void on_actionZakoncz_triggered();

     void on_actionAnaliza_triggered();

     void on_actionKalibracja_triggered();

     void on_actionTest_triggered();

     void on_OpenReference_clicked();

     void mouseRelease(QMouseEvent* event);

     void plotClick(QCPAbstractPlottable *plottable, QMouseEvent* event);

     void on_plusButton_2_clicked();

     void on_minusButton_2_clicked();

     void on_zeroButton_2_clicked();

     void on_zastapButton_clicked();

     void on_wyczyscButton_clicked();

     void on_actionPodpowiedzi_toggled(bool arg1);

     void on_gradientButton_clicked();

     void on_checkBoxTEMP_toggled(bool checked);

     void on_lineEditTEMP_valueChanged(double arg1);

     void on_captureImg_clicked();

     void on_actionKonfiguracja_triggered();

     void on_calibrateButton_clicked();
     void initModbus();
     void writePLC();
     void timeoutReplot();
       //zapisz dane do PLC

private Q_SLOTS:
     //sloty maszyny stanów:

     void stateInitSlot();

     void stateIdleSlot();

     void stateSTartCalibrateSlot();

     void stateLightsOnSlot();

     void stateCaptureImgCalSlot();

     void stateImageCapturedSlot();

     void stateIsCalibrateFinishedSlot();

     void stateSendSetpointsSlot();

    //koniec slotow maszyny stanow
    void onStateChanged(int state);

//    void coilChanged(int id);
//    void discreteInputChanged(int id);
//    void bitChanged(int id, QModbusDataUnit::RegisterType table, bool value);

    void setRegister(const int &value, const int &address);
    void updateData(QModbusDataUnit::RegisterType table, int address, int size);

//    //void on_connectType_currentIndexChanged(int);

    void handleDeviceError(QModbusDevice::Error newError);

//    void on_actionConnect_clicked();


    void on_pushZrobZdjecie_clicked();


    void on_pushButton_clicked();

    void on_actionLaboratorium_triggered();

    void on_reportButton_clicked();

    void on_checkBoxLightCal_toggled(bool checked);    

private:
    Ui::MainWindow *ui;

//    void initActions();
    void setupDeviceData();
//    void setupWidgetContainers();
    QModbusServer* modbusDevice;
    //StatesClass* states;
   // QButtonGroup coilButtons;
   // QButtonGroup discreteButtons;
   // QHash<QString, QLineEdit*> registers;
    //SettingsDialog *m_settingsDialog;
    void calculateParameter(double pomTr);
    void setDefaultGradients(int refPathNo);
};

#endif // MAINWINDOW_H
