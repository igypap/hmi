#ifndef KONFIGURACJA_H
#define KONFIGURACJA_H

#include <QDialog>
#include <QAbstractButton>
#include <opencv2/opencv.hpp>

namespace Ui {
  class Konfiguracja;
}

class Konfiguracja : public QDialog
{
  Q_OBJECT

public:
  explicit Konfiguracja(QWidget *parent = 0,
                        QVector<QString> _paths=QVector<QString>(2,""),
                        QVector<double> _pixels=QVector<double>(12,0), int _ID_Cam=0);
  ~Konfiguracja();

  cv::Mat map1; //Mapa X do funkcji prostującej obraz
  cv::Mat map2; //Mapa Y do funkcji prostującej obraz
  QString pathX; //ścieżka do pliku .CSV zawierającego mapę X
  QString pathY; //ścieżka do pliku .CSV zawierającego mapę Y
  QVector<double> pixels;
  QVector<int> minVec;
  QVector<int> maxVec;
    //pixele to wartości INT ale zwracamy DOUBLE dla funkcji liczącej wielomian
  int ID_Cam; // ID kamery w sieci
  bool status; //status konfiguracji: TRUE - skonfigurowano, FALSE - nie
  QVector<int> getMax();
    //metoda zwraca wektor wartości MAX gradientów
  QVector<int> getMin();
    //metoda zwraca wektor wartości MIN gradientów
  QVector<double> getValues();
    //metoda zwraca tablicę z wartościami pixeli dla danego czujnika temperatury
  QVector<QString> getPaths();
    //metoda zwraca skonfigurowane ścieżki do plików map transformacji
  void givePaths(QVector<QString> _paths);
    //metoda pobiera ścieżki do plików. Można wykorzystać konstruktor zamiast tej metody
  QVector<cv::Mat>getMaps();
    // metoda zwraca mapy transformacji
  bool getStatus();
    //metoda zwraca status konfiguracji (wykonana lub nie)
  int getID();
    // metoda zwraca ID kamery



private:
  void setMap(const cv::Mat _map1, const cv::Mat _map2);//przypisanie danych
  void setPaths(const QString _pathX, const QString _pathY);//przypisanie danych
  void openMaps();
    //załadowanie plików csv z mapami transformacji z dysku
  void openGradient();
    //załadowanie pliku csv z wartościami MIN MAX gradientów
  void setLabelText();
    //metoda pomocnicza

private slots:

  void on_spinBoxCh_valueChanged(int arg1);
    //obsługa UI. Wybór ilości kanałów
  void on_toolButtonVXQ_clicked();
    //wskazanie ścieżki do pliku csv dla X, MAP transformacji - nie pobiera danych z pliku
  void on_toolButtonVYQ_clicked();
    //wskazanie ścieżki do pliku csv dla Y, MAP transformacji - nie pobiera danych z pliku
  void on_pushZapisz_clicked();
    //załadowanie do pamięci danych z plików csv MAP transforamcji.
  void on_pushOK_clicked();
    //potwierdzenie przeprowadzenia konfiguracji i zamknięcie konfiguratora
  void on_pushAnuluj_clicked();
    //zaniechanie konfiguracji
  void on_spinBoxIDCam_valueChanged(int arg1);
    //wybór ID kamery
  void on_toolButtonGrad_clicked();

private:
  Ui::Konfiguracja *uikonf;
};

#endif // KONFIGURACJA_H
