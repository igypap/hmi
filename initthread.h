#ifndef INITTHREAD_H
#define INITTHREAD_H

#include <QThread>
#include<QVector>
#include "opencv2/opencv.hpp"

class initThread : public QThread
{

  Q_OBJECT

public:
  explicit initThread(QObject *parent=0);

  void run();
  void setPaths(QVector<QString> _paths);

  QVector<QString> paths;

signals:
  void updateProgressBar(int);
  void mapsLoaded(cv::Mat,cv::Mat, QVector<QVector<double>>,QVector<QVector<double>>,QVector<QVector<double>>,
                  QVector<QVector<double>>,QVector<QVector<double>>);
};

#endif // INITTHREAD_H
