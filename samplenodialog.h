#ifndef SAMPLENODIALOG_H
#define SAMPLENODIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
  class sampleNoDialog;
}

class sampleNoDialog : public QDialog
{
  Q_OBJECT

public:
  explicit sampleNoDialog(QWidget *parent = 0);
  ~sampleNoDialog();
  bool getStatus();
  QString getSampleNo();
  QString getOrderNo();

private slots:
  void on_pushButton_Ok_clicked();

  void on_pushButton_Anuluj_clicked();

  void on_lineEdit_textChanged(const QString &arg1);

  void on_lineEditOrder_textChanged(const QString &arg1);

private:
  Ui::sampleNoDialog *ui;
  QString sampleNo;
  QString orderNo;
  bool status;
};

#endif // SAMPLENODIALOG_H
