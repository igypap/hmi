#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,1,0,0
#define VER_FILEVERSION_STR         "2.1.0.0\0"

#define VER_PRODUCTVERSION          2,1,0,0
#define VER_PRODUCTVERSION_STR      "2.1\0"

#define VER_COMPANYNAME_STR         "CHIMECA"
#define VER_FILEDESCRIPTION_STR     "System kalibracyjny"
#define VER_INTERNALNAME_STR        "System kalibracyjny"
#define VER_LEGALCOPYRIGHT_STR      "igorpaprzycki.github.io"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "HMI.exe"
#define VER_PRODUCTNAME_STR         "System kalibracyjny"

#define VER_COMPANYDOMAIN_STR       "igorpaprzycki.github.io"

#endif // VERSION_H
