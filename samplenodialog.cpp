#include "samplenodialog.h"
#include "ui_samplenodialog.h"

bool valueOkSampleNo=false;
bool valueOkOrder=false;
sampleNoDialog::sampleNoDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::sampleNoDialog)
{
  ui->setupUi(this);
  sampleNo="";
  orderNo="";
  status=false;
  ui->pushButton_Ok->setEnabled(false);
}

sampleNoDialog::~sampleNoDialog()
{
  delete ui;
}

bool sampleNoDialog::getStatus()
{
  return status;
}

QString sampleNoDialog::getSampleNo()
{
  return sampleNo;
}

QString sampleNoDialog::getOrderNo(){
  return orderNo;
}

void sampleNoDialog::on_pushButton_Ok_clicked()
{
  sampleNo=ui->lineEdit->text();
  orderNo=ui->lineEditOrder->text();
  status=true;
  emit accept();
}

void sampleNoDialog::on_pushButton_Anuluj_clicked()
{
  emit reject();
}

void sampleNoDialog::on_lineEdit_textChanged(const QString &arg1)
{
  if (arg1.size()>2){
      valueOkSampleNo=true;
      if (valueOkSampleNo&&valueOkOrder){
          ui->pushButton_Ok->setEnabled(true);
        }else{
          ui->pushButton_Ok->setEnabled(false);
        }
    }else{
      valueOkSampleNo=false;
      ui->pushButton_Ok->setEnabled(false);
    }
}

void sampleNoDialog::on_lineEditOrder_textChanged(const QString &arg1)
{
  if (arg1.size()>2){
      valueOkOrder=true;
      if (valueOkSampleNo&&valueOkOrder){
          ui->pushButton_Ok->setEnabled(true);
        }else{
          ui->pushButton_Ok->setEnabled(false);
        }
    }else{
      valueOkOrder=false;
      ui->pushButton_Ok->setEnabled(false);
    }
}
