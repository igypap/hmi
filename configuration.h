#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
  class Configuration;
}

class Configuration : public QDialog
{
  Q_OBJECT

public:
  explicit Configuration(QWidget *parent = 0);
  ~Configuration();
  void setConfigData(ConfigClass *_Config);
  bool getStatus();

private slots:
  void on_pushButton_Ok_clicked();

  void on_pushButton_Anuluj_clicked();

  void on_toolButton_Folia_I_clicked();

  void on_toolButton_Folia_Ia_clicked();

  void on_toolButton_Folia_II_clicked();

  void on_toolButton_Dane_clicked();

  void on_toolButton_Raporty_clicked();

private:
  Ui::Configuration *ui;
  bool status;
  ConfigClass *config;
};

#endif // CONFIGURATION_H
