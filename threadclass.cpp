#include "threadclass.h"
#include <QtCore>
#include "IDSCamera.h"
#include "mainwindow.h"
#include "opencv2/opencv.hpp"



threadClass::threadClass(QObject *parent):QThread(parent)
{

}

void threadClass::run()
{
  IDSCamera cam(ID,map1,map2,lightCalB, lightCalG, lightCalR, option, NULL,IS_PARAMETERSET_CMD_LOAD_EEPROM);
  cv::Mat img = cam.captureAndRectifyImage();//captureAndRectifyImage();

  emit ImageCaptured(img);
}

void threadClass::setConfig(int _ID, cv::Mat _map1, cv::Mat _map2,
                            QVector<QVector<double>> _lightCalB,
                            QVector<QVector<double>> _lightCalG,
                            QVector<QVector<double>> _lightCalR,
                            int _option)
{
  ID=_ID;
  map1=_map1;
  map2=_map2;
  lightCalB=_lightCalB;
  lightCalG=_lightCalG;
  lightCalR=_lightCalR;
  option=_option;
}
