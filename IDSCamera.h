#pragma once
#include "uEye.h"
#include <opencv2\core\core.hpp>
#include <QtCore>

class IDSCamera
{
public:
	IDSCamera();
	IDSCamera(const INT _camID, const cv::Mat _map1, const cv::Mat _map2, QVector<QVector<double> > _lightCalB, QVector<QVector<double> > _lightCalG, QVector<QVector<double> > _lightCalR, INT _option, wchar_t* _iniPath, UINT _paramsLoadingMode);
	~IDSCamera();
	cv::Mat captureImage(); // funkcja wykonuj�ca zdj�cie i konwertuj�ca je do postaci Mat z openCV. Zwraca pusty obiekt Mat,
							//je�li nie uda�o si� wykona� zdj�cia.
	cv::Mat captureAndRectifyImage(); // funkcja wykonuj�ca zdj�cie i "prostuj�ca" je. Na wyj�ciu wyprostowany obraz.
									  // Zwraca pusty obiekt mat, je�li nie uda�o si� wykona� zdj�cia (np. przez zerwanie komunikacji z kamer�)
									  // lub wymiary zdj�cia nie odpowiadaj� wczytanym macierzom kalibracyjnym m_map1 i m_map2.

	// settery/gettery zmiennych m_map1 i m_map2
	void setMap1(const cv::Mat _map1);
	void setMap2(const cv::Mat _map2);
	cv::Mat getMap1();
	cv::Mat getMap2();

	void setIniPath(wchar_t* _iniPath);
	wchar_t* getIniPath();

	// odpytanie o wynik ostatniej pr�by po��czenia z kamer�
	INT getCameraStatus() { return m_nCameraStatus; };

private:
	HIDS	m_hCam;			// obiekt reprezentuj�cy kamer�.
	INT		m_nCameraStatus;// status po��czenia z kamer�;
	INT		m_nColorMode;	// Y8/RGB16/RGB24/REG32.
	INT		m_nBitsPerPixel;// liczba bit�w na piksel.
	INT		m_nSizeX;		// szeroko�� obrazu (ustalana na podstawie informacji z oprogramowania kamery).
	INT		m_nSizeY;		// wysoko�� obrazu (ustalana na podstawie informacji z oprogramowania kamery).
	INT		m_lMemoryId;	// ID bufora pami�ci przechowuj�cego obrazy.
	INT		m_nCameraID;	// ID kamery, s�u��ce do jej jednoznacznej identyfikacji w sieci. Mo�na je zmieni� np. z poziomu programu IDS Camera Manager.
	char*	m_pcImageMemory;// wska�nik do bufora opisanego wy�ej.
	SENSORINFO m_sInfo;	    // informacje o matrycy pozyskane poprzez odpytanie kamery.
	UINT m_nParamsLoadingMode; //informacja o sposobie �adowania parametr�w kamery (patrz: funkcja loadCameraParameters).

	wchar_t* m_sIniPath; // �cie�ka (relatywna lub absolutna) do pliku konfiguracyjnego INI (musi wskazywa� na konkretny plik INI, nie na katalog).

	cv::Mat m_map1; // pierwsza macierz przekszta�ce�, potrzebna do "wyprostowania" obrazu.
	cv::Mat m_map2; // druga macierz przekszta�ce�, potrzebna do "wyprostowania" obrazu.
	QVector<QVector<double>> m_lightCalB;
	QVector<QVector<double>> m_lightCalG;
	QVector<QVector<double>> m_lightCalR;
	INT m_option;

	INT initCamera(HIDS *_hCam, HWND _hWnd); // funkcja inicjalizuj�ca podstawowe parametry kamery.
	bool openCamera(); // uzyskanie dost�pu do pami�ci kamery, pobranie zdj�cia i zapis do bufora m_pcImageMemory.
	INT loadCameraParameters(UINT _command); //funkcja �aduj�ca parametry kamery z pliku INI lub pami�ci EEPROM kamery.
												// _command = IS_PARAMETERSET_CMD_LOAD_FILE dla �adowania z pliku INI;
												// _command = IS_PARAMETERSET_CMD_LOAD_EEPROM dla �adowania z EEPROM.
												//! funkcja ta jest wywo�ana przed ka�dym wykonaniem zdj�cia, aby zapewni�
												// maksymaln� "bezobs�ugowo��" w razie chwilowej utraty po��czenia.

	void getMaxImageSize(INT *_pnSizeX, INT *_pnSizeY); // pozyskanie informacji o maksymalnych wymiarach zdj�cia poprzez odpytanie kamery.

};

