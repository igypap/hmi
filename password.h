#ifndef PASSWORD_H
#define PASSWORD_H

#include <QDialog>

namespace Ui {
  class Password;
}

class Password : public QDialog
{
  Q_OBJECT

public:
  explicit Password(QWidget *parent = 0);
  ~Password();

  bool getStatus();
  void setPassword(QString _pass);

private slots:
  void on_pushButton_Ok_clicked();

  void on_lineEdit_textChanged(const QString &arg1);

  void on_pushButton_Anuluj_clicked();

private:
  Ui::Password *ui;
  bool status;
  QString pass;
};

#endif // PASSWORD_H
