#ifndef REFERENCEDIALOG_H
#define REFERENCEDIALOG_H

#include <QDialog>

namespace Ui {
  class ReferenceDialog;
}

class ReferenceDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ReferenceDialog(QWidget *parent = 0);
  ~ReferenceDialog();
  int getRefPathNo();
  bool getStatus();

private slots:
  void on_pushButton_clicked();

  void on_pushButton_2_clicked();

private:
  Ui::ReferenceDialog *ui;
  int refPathNo;
  bool status;
};

#endif // REFERENCEDIALOG_H
