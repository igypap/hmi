#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QDialog>
#include <QVector>


namespace Ui {
  class Parameters;
}

class Parameters : public QDialog
{
  Q_OBJECT

public:
  explicit Parameters(QWidget *parent = 0);
  ~Parameters();
  QVector<double> getParameters();
  void setParameters(QVector<double> _params);
  bool getStatus();


private slots:
  void on_pushButton_Ok_clicked();

  void on_pushButton_Anuluj_clicked();

private:
  Ui::Parameters *ui;
  double cMin, cMax, cWindow, cStep;
  bool status;
};

#endif // PARAMETERS_H
